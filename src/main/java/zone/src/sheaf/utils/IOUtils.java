/*
 * Copyright 2015 by The.Src.Zone (Amsterdam, The Netherlands)  
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except 
 * in compliance with the License. You may obtain a copy of the License at
 * 
 * http://the.src.zone/licenses/apache-2.0 
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the License 
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */
package zone.src.sheaf.utils;

import java.io.Closeable;
import java.io.IOException;

import jakarta.annotation.Nullable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.io.Closeables;

public final class IOUtils {
    private static final Logger LOG = LoggerFactory.getLogger(IOUtils.class);
    
    /**
     * Silently closes a {@link Closeable}: any IOException thrown during the close will be logged
     * and swallowed.
     * In most cases you don't want to do this, but use {@link Closeables#close(Closeable, boolean)}
     * instead, to throw the IOException unless that will hide an original IOException thrown before
     * the close. See the example code in {@link Closeables#close(Closeable, boolean)}
     *
     * @param closeable the {@code Closeable} to be closed, or null.
     */
    public static void close(@Nullable Closeable closeable) {
      if (closeable == null)
        return;
      try {
        closeable.close();
      } catch (IOException e) {
          LOG.error("IOException ignored on close of Closeable", e);
      }
    }
}