/*
 * Copyright 2014 by The.Src.Zone (Amsterdam, The Netherlands)  
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except 
 * in compliance with the License. You may obtain a copy of the License at
 * 
 * http://the.src.zone/licenses/apache-2.0 
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the License 
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */
package zone.src.sheaf.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.net.URL;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.util.Map;

import zone.src.sheaf.constants.TextEncodings;
import zone.src.sheaf.properties.ApplicationProperties;

import com.google.common.io.CharStreams;

/**
 * Various utility methods for handling strings.
 */
public final class StringUtils {
    /**
     * Evaluates a string expression to a final string. The expression may
     * contain expressions starting with a <code>$</code> sign:
     * <ul>
     * <li><code>$variable</code>: gets substituted by the value of
     * <code>variable</code>, or if there is no variable with that name, by an
     * empty string.</li>
     * <li><code>${variable}</code>: gets substituted by the value of
     * <code>variable</code>, or if there is no variable with that name, by an
     * empty string.</li>
     * <li><code>${variable:empty}</code>: gets substituted by the value of
     * <code>variable</code>, or if there is no variable with that name, by the
     * string following the colon (<code>'empty'</code> in this example).</li>
     * </ul>
     * 
     * @param expression
     *            The expression to process (may not be null)
     * @param substitutes
     *            The values of the variables to use during evaluation (or null
     *            for no variables)
     * @return The evaluated string
     */
    public static String evaluate(String expression, Map<? extends CharSequence, ? extends CharSequence> substitutes) {
        return ApplicationProperties.evaluateString(expression, substitutes);
    }

    /**
     * URL-Decode the given string using UTF-8. See
     * {@link URLDecoder#decode(String, String)}.
     * 
     * @param str
     *            The input string
     * @return The decoded string.
     */
    public static String urlEncode(String str) {
        return urlEncode(str, TextEncodings.UTF8);
    }
    
    static String urlEncode(String str, String enc) {
        try {
            return URLEncoder.encode(str, enc);
        } catch (UnsupportedEncodingException e) {
            throw new IllegalStateException(e);
        }
    }

    /**
     * URL-Decode the given string using UTF-8. See
     * {@link URLDecoder#decode(String, String)}.
     * 
     * @param str
     *            The input string
     * @return The decoded string.
     */
    public static String urlDecode(String str) {
        return urlDecode(str, TextEncodings.UTF8);
    }
    
    static String urlDecode(String str, String enc) {
        try {
            return URLDecoder.decode(str, enc);
        } catch (UnsupportedEncodingException e) {
            throw new IllegalStateException(e);
        }
    }
    
    /**
     * Read a file into a string.
     * 
     * @param file The file
     * @param charset The character set to use when reading the file
     * 
     * @return The string contents of the file
     * 
     * @see #toString(InputStream, Charset, boolean)
     * 
     * @throws IOException On IO errors
     */
    public static String toString(File file, Charset charset) throws IOException {
        return toString(new FileInputStream(file), charset, true);
    }

    /**
     * Read a stream into a string, and make sure the stream is closed
     * afterwards.
     * 
     * @param stream The stream
     * @param charset The character set to use when reading the stream
     * 
     * @return The string contents of the stream
     * 
     * @see #toString(InputStream, Charset, boolean)
     * 
     * @throws IOException On IO errors     */
    public static String toString(InputStream stream, Charset charset) throws IOException {
        return toString(stream, charset, true);
    }

    /**
     * Read a stream into a string.
     * 
     * @param stream
     *            The stream from which input characters should be read.
     * @param charset
     *            The encoding to use for reading the input stream
     * @param close
     *            True when the stream should be closed by this method afterwards, false
     *            if the caller handles closing of the stream.
     * @return A string that contains all characters from the input stream
     * @throws IOException On IO Error
     */
    public static String toString(InputStream stream, Charset charset, boolean close) throws IOException {
        return toString(new InputStreamReader(stream, charset), close);
    }
    
    public static String toString(Reader reader) throws IOException {
        return toString(reader, true);
    }
    
    @SuppressWarnings({
        "PMD.DoNotThrowExceptionInFinally", // only throwing when we are not already throwing
        "PMD.UnnecessaryLocalBeforeReturn"  // false positive
    }) public static String toString(Reader reader, boolean close) throws IOException {
        boolean succes = false;
        try {
            String result = CharStreams.toString(reader);
            succes = true;
            return result;
        } finally {
            if (close) {
                try {
                    reader.close();
                } catch (IOException e) {
                    if (succes)
                        throw e;
                    // else we are already throwing an exception!
                }
            }
        }
    }

    /**
     * Read a stream from a given URL into a string, and make sure the stream is
     * closed afterwards.
     *
     * @param url The url to read from
     * @param charset The character set to use when reading from the url
     * 
     * @return The string content read from the url
     * 
     * @see #toString(InputStream, Charset, boolean)
     * 
     * @throws IOException On IO errors     */
    public static String toString(URL url, Charset charset) throws IOException {
        return toString(url.openStream(), charset, true);
    }
    
    /**
     * Write a string to a file
     * 
     * @param string The string to output
     * @param file
     *            The file to write to
     * @param charset
     *            The encoding to use for writing to the stream
     * 
     * @throws IOException On IO errors
     */
    public static void toFile(String string, File file, Charset charset) throws IOException {
        toStream(string, new FileOutputStream(file), charset, true);
    }
    
    /**
     * Write a string to a stream, and make sure the stream is closed
     * afterwards.
     * 
     * @param string The string to write
     * @param stream The stream to write to
     * @param charset The character set to use for writing
     * 
     * @see #toStream(String, OutputStream, Charset, boolean)
     * 
     * @throws IOException On IO errors
     */
    public static void toStream(String string, OutputStream stream, Charset charset) throws IOException {
        toStream(string, stream, charset, true);
    }

    /**
     * Write a string to a stream.
     * 
     * @param string The string to output
     * @param stream
     *            The stream to write to
     * @param charset
     *            The encoding to use for writing to the stream
     * @param close
     *            True when the stream should be closed by this method afterwards, false
     *            if the caller handles closing of the stream.
     *            
     * @throws IOException On IO errors
     */
    public static void toStream(String string, OutputStream stream, Charset charset, boolean close) throws IOException {
        toWriter(string, new OutputStreamWriter(stream, charset), close);
    }
    
    public static void toWriter(String string, Writer writer) throws IOException {
        toWriter(string, writer, true);
    }
    
    @SuppressWarnings("PMD.DoNotThrowExceptionInFinally")
    public static void toWriter(String string, Writer writer, boolean close) throws IOException {
        boolean succes = false;
        try {
            writer.append(string);
            succes = true;
        } finally {
            if (close) {
                try {
                    writer.close();
                } catch (IOException e) {
                    if (succes)
                        throw e;
                    // else we are already throwing an exception!
                }
            }
        }
    }
    
    /**
     * Remove the first and last character from a string if they both equal the single or the double quote character.
     * Return the original string if that's not the case.
     * @param string The string to unquote.
     * @return The unquoted string.
     */
    public static String unquote(String string) {
        if (string == null)
            return null;
        int len = string.length();
        if (len >= 2) {
            char first = string.charAt(0);
            if ((first == '\'' || first == '\"') && string.charAt(len - 1) == first)
                return string.substring(1, len - 1);
        }
        return string;
    }
}