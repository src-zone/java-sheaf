/*
 * Copyright 2014 by The.Src.Zone (Amsterdam, The Netherlands)  
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except 
 * in compliance with the License. You may obtain a copy of the License at
 * 
 * http://the.src.zone/licenses/apache-2.0 
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the License 
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */
package zone.src.sheaf.utils;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.attribute.FileAttribute;
import java.nio.file.attribute.PosixFilePermission;
import java.nio.file.attribute.PosixFilePermissions;
import java.util.Set;

import com.google.common.collect.ImmutableSet;

import zone.src.sheaf.utils.io.RecursiveDeleteFileVistor;

/**
 * Various utility methods for handling files, paths, and i/o.
 * <p>
 * For i/o of String's, see {@link StringUtils}
 */
public final class FileUtils {
	public static final boolean IS_POSIX_ALLOWED = isPosixAllowed();
	public static final FileAttribute<?>[] USER_RW = asPermissions("rw-------");
	public static final FileAttribute<?>[] USER_RWX = asPermissions("rwx------");
    
    /**
     * Recursively delete a file or directory (i.e. delete any subdirectories and files within).
     * @param file The file or directory to delete
     * @throws IOException On IO errors
     */
    public static void deleteRecursive(File file) throws IOException {
        deleteRecursive(file.toPath());
    }
    
    /**
     * Recursively delete a file or directory (i.e. delete any subdirectories and files within).
     * @param path The file or directory to delete
     * @throws IOException On IO errors
     */
    public static void deleteRecursive(Path path) throws IOException {
        Files.walkFileTree(path, new RecursiveDeleteFileVistor());
    }
    
    public static FileAttribute<?>[] asPermissions(String posixPerms) {
    	if (isPosixAllowed()) {
    		Set<PosixFilePermission> perms = ImmutableSet.copyOf(PosixFilePermissions.fromString("rw-------"));
    		return new FileAttribute<?>[] { PosixFilePermissions.asFileAttribute(perms) };
    	}
    	return new FileAttribute<?>[0];
    }
    
    private static boolean isPosixAllowed() {
    	Set<PosixFilePermission> perms = ImmutableSet.copyOf(PosixFilePermissions.fromString("rw-------"));
    	FileAttribute<Set<PosixFilePermission>> permissions = PosixFilePermissions.asFileAttribute(perms);
    	try {
    		Path file = Files.createTempFile("tmp-posix", ".tmp", permissions);
    		Files.delete(file);
    		return true;
    	} catch (UnsupportedOperationException e) {
    		return false;
    	} catch (IOException e) {
    		throw new IllegalStateException(e);
    	}
    }
}