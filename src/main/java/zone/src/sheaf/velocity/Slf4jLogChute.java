/*
 * Copyright 2014 by The.Src.Zone (Amsterdam, The Netherlands)  
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except 
 * in compliance with the License. You may obtain a copy of the License at
 * 
 * http://the.src.zone/licenses/apache-2.0 
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the License 
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */
package zone.src.sheaf.velocity;

import org.apache.velocity.runtime.RuntimeConstants;
import org.apache.velocity.runtime.RuntimeServices;
import org.apache.velocity.runtime.log.LogChute;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import zone.src.sheaf.properties.ApplicationProperties;

import com.google.common.base.Preconditions;

/**
 * Pipes velocity log messages to Slf4J.
 * <p>
 * {@link ApplicationProperties} will by default initialize {@link RuntimeConstants#RUNTIME_LOG_LOGSYSTEM_CLASS} to this class to force Velocity to use this as logger.
 */
public class Slf4jLogChute implements LogChute {
    private static final Logger LOG = LoggerFactory.getLogger(Slf4jLogChute.class);

    @Override public void init(RuntimeServices rs) {
        Preconditions.checkNotNull(LOG);
    }

    @Override public void log(int level, String message) {
        switch (level) {
        case LogChute.ERROR_ID:
            LOG.error(message);
            break;
        case LogChute.WARN_ID:
            LOG.warn(message);
            break;
        case LogChute.INFO_ID:
            LOG.info(message);
            break;
        case LogChute.TRACE_ID:
            LOG.trace(message);
            break;
        case LogChute.DEBUG_ID:
        default:
            LOG.debug(message);
            break;
        }
    }

    @Override public void log(int level, String message, Throwable t) {
        switch (level) {
        case LogChute.ERROR_ID:
            LOG.error(message, t);
            break;
        case LogChute.WARN_ID:
            LOG.warn(message, t);
            break;
        case LogChute.INFO_ID:
            LOG.info(message, t);
            break;
        case LogChute.TRACE_ID:
            LOG.trace(message, t);
            break;
        case LogChute.DEBUG_ID:
        default:
            LOG.debug(message, t);
            break;
        }
    }

    @Override public boolean isLevelEnabled(int level) {
        switch (level) {
        case LogChute.ERROR_ID:
            return LOG.isErrorEnabled();
        case LogChute.WARN_ID:
            return LOG.isWarnEnabled();
        case LogChute.INFO_ID:
            return LOG.isInfoEnabled();
        case LogChute.TRACE_ID:
            return LOG.isTraceEnabled();
        case LogChute.DEBUG_ID:
        default:
            return LOG.isDebugEnabled();
        }
    }
}
