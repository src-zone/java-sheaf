/*
 * Copyright 2015 by The.Src.Zone (Amsterdam, The Netherlands)  
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except 
 * in compliance with the License. You may obtain a copy of the License at
 * 
 * http://the.src.zone/licenses/apache-2.0 
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the License 
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */
package zone.src.sheaf.template.hierarchy;

import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashSet;

import zone.src.sheaf.template.TemplateBindStrategy;

import com.google.common.base.Preconditions;

public final class TemplateBindNode<T extends Object> {
    private Class<? extends T> clazz;
    private TemplateBindStrategy strategy;
    private URL templateUrl;
    private TemplateBindNode<T> parent;
    private Collection<TemplateBindNode<T>> children;
    private Collection<T> leafs;
    
    TemplateBindNode(Class<? extends T> clazz, TemplateBindStrategy strategy, URL templateUrl) {
        this.clazz = clazz;
        this.strategy = strategy;
        this.templateUrl = templateUrl;
    }
    
    public Class<? extends T> getClazz() {
        return clazz;
    }
    
    public TemplateBindStrategy getStrategy() {
        return strategy;
    }
    
    public URL getTemplateUrl() {
        return templateUrl;
    }

    public TemplateBindNode<T> getParent() {
        return parent;
    }

    public Iterable<TemplateBindNode<T>> getChildren() {
        return children == null ? Collections.emptyList() : Collections.unmodifiableCollection(children);
    }

    void setParent(TemplateBindNode<T> parent) {
        Preconditions.checkArgument(this.parent == null || this.parent == parent);
        Preconditions.checkNotNull(parent);
        this.parent = parent;
    }
    
    void addChild(TemplateBindNode<T> child) {
        if (children == null)
            children = new LinkedHashSet<>();
        children.add(child);
    }
    
    public Iterable<T> getAllLeafs() {
        ArrayList<T> result = new ArrayList<>();
        addAllLeafs(result, this);
        return result;
    }

    protected static <T> void addAllLeafs(Collection<T> result, TemplateBindNode<T> node) {
        if (node.leafs != null)
            for (T leaf: node.leafs)
                result.add(leaf);
        if (node.children != null)
            for (TemplateBindNode<T> child: node.children)
                addAllLeafs(result, child);
    }
    
    public void addLeaf(T leaf) {
        if (leafs == null)
            leafs = new LinkedHashSet<>();
        leafs.add(leaf);
    }

    @Override public int hashCode() {
        return clazz.hashCode();
    }

    @Override public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj instanceof TemplateBindNode) {
            TemplateBindNode<?> other = (TemplateBindNode<?>)obj;
            return clazz.equals(other.clazz) && strategy.equals(other.strategy);
        }
        return false;
    }

    @Override public String toString() {
        String parentStr = parent == null ? null : parent.clazz.getSimpleName();
        return "TemplateBindNode [clazz=" + clazz.getSimpleName() + ", strategy=" + strategy + ", templateUrl=" + templateUrl + ", parent=" + parentStr + "]";
    }
}