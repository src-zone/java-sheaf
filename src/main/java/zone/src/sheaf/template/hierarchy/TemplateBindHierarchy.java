/*
 * Copyright 2015 by The.Src.Zone (Amsterdam, The Netherlands)  
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except 
 * in compliance with the License. You may obtain a copy of the License at
 * 
 * http://the.src.zone/licenses/apache-2.0 
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the License 
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */
package zone.src.sheaf.template.hierarchy;

import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import zone.src.sheaf.template.TemplateBindStrategy;
import zone.src.sheaf.template.TemplateType;

import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;

/**
 * Looks for templates (see {@link TemplateBindStrategy}) for all classes in the
 * class-hierarchy of an object.
 * 
 * @param <T>
 *            The base class (root) of this hierarchy. Superclasses of the root
 *            will not be used for matching template lookups.
 */
public final class TemplateBindHierarchy<T extends Object> {
    private final Class<T> rootClass;
    private final TemplateType templateType;
    private final Map<T, TemplateBindNode<T>> leafs = new LinkedHashMap<>();
    private final Map<TemplateBindNode<T>, TemplateBindNode<T>> nodes = new HashMap<>();
    
    public static <T extends Object> TemplateBindHierarchy<T> create(Class<T> rootClass, TemplateType templateType) {
        return new TemplateBindHierarchy<>(rootClass, templateType);
    }

    private TemplateBindHierarchy(Class<T> rootClass, TemplateType templateType) {
        this.rootClass = rootClass;
        this.templateType = templateType;
    }

    @SuppressWarnings("unchecked") public TemplateBindHierarchy<T> add(TemplateBindStrategy strategy, T object) {
        Preconditions.checkNotNull(strategy);
        Preconditions.checkNotNull(object);
        Preconditions.checkArgument(!leafs.containsKey(object), "duplicate add: the object %s is already added as leaf", object);
        Class<T> clazz = (Class<T>)object.getClass();
        TemplateBindNode<T> childNode = null;
        TemplateBindNode<T> node = createNextNode(strategy, clazz);
        Preconditions.checkNotNull(node, "No template to bind to type %s", clazz);
        leafs.put(object, node);
        node.addLeaf(object);
        do {
            if (childNode != null) {
                node.addChild(childNode);
                childNode.setParent(node);
            }
            nodes.put(node, node);
            childNode = node;
            node = createNextNode(strategy, (Class<? extends T>)nextClass(node.getClazz()));
        } while (node != null);
        return this;
    }
    
    /**
     * @return The added (by {@link #add(TemplateBindStrategy, Object)}) objects in the order they where added.
     */
    public Iterable<T> getLeafs() {
        return leafs.keySet();
    }
    
    public int countLeafs() {
        return leafs.keySet().size();
    }

    public Iterable<TemplateBindNode<T>> getBindChain(T object) {
        Preconditions.checkNotNull(object);
        ArrayList<TemplateBindNode<T>> result = new ArrayList<>();
        TemplateBindNode<T> node = leafs.get(object);
        while (node != null) {
            result.add(node);
            node = node.getParent();
        }
        return Lists.reverse(result);
    }

    public TemplateBindNode<T> getBindRoot(T object) {
        Preconditions.checkNotNull(object);
        TemplateBindNode<T> node = leafs.get(object);
        if (node == null)
            return null;
        TemplateBindNode<T> parent = node.getParent();
        while (parent != null) {
            node = parent;
            parent = node.getParent();
        }
        return node;
    }

    @SuppressWarnings("unchecked") protected <U extends T> TemplateBindNode<T> createNextNode(TemplateBindStrategy strategy, Class<U> clazz) {
        URL url = null;
        while (url == null && clazz != null) {
            url = strategy.getTemplate(clazz, templateType);
            if (url == null)
                clazz = (Class<U>)nextClass(clazz);
        }
        if (url == null)
            return null;
        TemplateBindNode<T> node = new TemplateBindNode<T>(clazz, strategy, url);
        TemplateBindNode<T> matchedNode = nodes.get(node);
        return matchedNode == null ? node : matchedNode;
    }
    
    private Class<?> nextClass(Class<?> clazz) {
        if (rootClass.equals(clazz))
            return null;
        return clazz.getSuperclass();
    }
}