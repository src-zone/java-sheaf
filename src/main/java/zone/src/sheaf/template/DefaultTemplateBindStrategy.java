/*
 * Copyright 2015 by The.Src.Zone (Amsterdam, The Netherlands)  
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except 
 * in compliance with the License. You may obtain a copy of the License at
 * 
 * http://the.src.zone/licenses/apache-2.0 
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the License 
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */
package zone.src.sheaf.template;

import java.net.URL;

/**
 * A default {@link TemplateBindStrategy} that looks for a file with the same name
 * as the provided class, in the same package, with a file extension matching
 * the {@link TemplateType}.
 */
public class DefaultTemplateBindStrategy implements TemplateBindStrategy {
    /**
     * @param clazz The class used for template lookup, as used in {@link #getTemplate(Class, TemplateType)}
     * @return The required name of a matching template, excluding the suffix/extension.
     */
    protected String getTemplateBaseName(Class<?> clazz) {
        return clazz.getSimpleName();
    }
    
    @Override public URL getTemplate(Class<?> clazz, TemplateType templateType) {
        String suffix = templateType.getSuffix();
        return clazz.getResource(getTemplateBaseName(clazz) + suffix);
    }

    @Override public String toString() {
        return "DefaultTemplateBindStrategy []";
    }

    @Override public int hashCode() {
        return DefaultTemplateBindStrategy.class.hashCode();
    }

    @Override public boolean equals(Object obj) {
        return (obj != null && obj.getClass().equals(this.getClass()));
    }
}