/*
 * Copyright 2015 by The.Src.Zone (Amsterdam, The Netherlands)  
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except 
 * in compliance with the License. You may obtain a copy of the License at
 * 
 * http://the.src.zone/licenses/apache-2.0 
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the License 
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */
package zone.src.sheaf.template;

import java.net.URL;

import zone.src.sheaf.template.hierarchy.TemplateBindHierarchy;

/**
 * <p>
 * A strategy for binding template files to objects and classes. An
 * implementation will associate a template file with a given class, or return
 * null if no template file is associated with the class.
 * </p>
 * <p>
 * A TemplateBindStrategy must NOT look for templates in superclasses. Use
 * {@link TemplateBindHierarchy} to search for matching templates in all levels
 * of a class hierarchy.
 * </p>
 * Implementations should properly implement <code>hashCode()</code> and
 * <code>equals(Object)</code>. TemplateBindStrategies should only be equal if their
 * {@link #getTemplate(Class, TemplateType)} implementation gives the same
 * result for the same arguments.
 */
public interface TemplateBindStrategy {
    /**
     * @param clazz
     *            The class to find an associated template for.
     * @param templateType
     *            The type of template to find.
     * @return An URL to the associated template, or null if there is no
     *         matching template for the given class
     */
    URL getTemplate(Class<?> clazz, TemplateType templateType);
}