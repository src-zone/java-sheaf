/*
 * Copyright 2015 by The.Src.Zone (Amsterdam, The Netherlands)  
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except 
 * in compliance with the License. You may obtain a copy of the License at
 * 
 * http://the.src.zone/licenses/apache-2.0 
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the License 
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */
package zone.src.sheaf.template;

import zone.src.sheaf.constants.FileExtensions;

/**
 * Types of templates, as used by {@link TemplateBindStrategy}.
 */
public enum TemplateTypes implements TemplateType {
    JAVASCRIPT {
        @Override public String getSuffix() {
            return FileExtensions.JS_SUFFIX;
        }
    },
    LESS {
        @Override public String getSuffix() {
            return FileExtensions.LESS_SUFFIX;
        }
    },
    HTML {
        @Override public String getSuffix() {
            return FileExtensions.HTML_SUFFIX;
        }
    };
}
