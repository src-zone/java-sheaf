/*
 * Copyright 2014 by The.Src.Zone (Amsterdam, The Netherlands)  
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except 
 * in compliance with the License. You may obtain a copy of the License at
 * 
 * http://the.src.zone/licenses/apache-2.0 
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the License 
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */
package zone.src.sheaf.asm;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import org.objectweb.asm.AnnotationVisitor;
import org.objectweb.asm.ClassReader;
import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.FieldVisitor;
import org.objectweb.asm.Handle;
import org.objectweb.asm.Label;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.Type;
import org.objectweb.asm.TypePath;
import org.objectweb.asm.signature.SignatureReader;
import org.objectweb.asm.signature.SignatureVisitor;

/**
 * Analyze dependencies of a class. See http://www.onjava.com/pub/a/onjava/2005/08/17/asm3.html?page=6
 */
public class DependencyVisitor extends ClassVisitor {
    private Set<String> dependencies = new HashSet<String>();

    public DependencyVisitor() {
        super(Opcodes.ASM5);
    }

    public static Set<String> getDependencies(Class<?> clazz) throws IOException {
        DependencyVisitor depVisitor = new DependencyVisitor();
        new ClassReader(clazz.getName()).accept(depVisitor, 0);
        return depVisitor.dependencies;
    }

    @Override
    public void visit(int version, int access, String name, String signature, String superName, String[] interfaces) {
        if (signature != null)
            addSignature(signature);
        else {
            addCanonicalName(superName);
            addCanonicalNames(interfaces);
        }
    }

    @Override
    public AnnotationVisitor visitAnnotation(String desc, boolean visible) {
        addDesc(desc);
        return new AnnotationDependencyVisitor();
    }

    @Override
    public AnnotationVisitor visitTypeAnnotation(int typeRef, TypePath typePath, String desc, boolean visible) {
        addDesc(desc);
        return new AnnotationDependencyVisitor();
    }

    @Override
    public FieldVisitor visitField(int access, String name, String desc, String signature, Object value) {
        if (signature != null)
            addTypeSignature(signature);
        else
            addDesc(desc);
        if (value instanceof Type)
            addType((Type)value);
        return new FieldDependencyVisitor();
    }

    @Override
    public MethodVisitor visitMethod(int access, String name, String desc, String signature, String[] exceptions) {
        if (signature != null)
            addSignature(signature);
        else {
            addMethod(desc);
            addCanonicalNames(exceptions);
        }
        return new MethodDependencyVisitor();
    }

    private void addDependency(String name) {
        if (name != null)
            dependencies.add(name.replace('/', '.'));
    }
    
    void addDesc(String desc) {
        addType(Type.getType(desc));
    }

    void addMethod(String desc) {
        addType(Type.getReturnType(desc));
        for (Type type: Type.getArgumentTypes(desc))
            addType(type);
    }

    void addCanonicalName(String name) {
        if (name != null)
            addType(Type.getObjectType(name));
    }
    
    void addCanonicalNames(String[] names) {
        if (names != null)
            for (String name: names)
                addType(Type.getObjectType(name));
    }


    void addType(Type t) {
        switch (t.getSort()) {
        case Type.ARRAY:
            addType(t.getElementType());
            break;
        case Type.OBJECT:
            addDependency(t.getInternalName());
            break;
        case Type.METHOD:
            addMethod(t.getDescriptor());
            break;
        }
    }

    private void addSignature(String signature) {
        if (signature != null)
            new SignatureReader(signature).accept(new SignatureDependencyVisitor());
    }

    void addTypeSignature(String signature) {
        if (signature != null)
            new SignatureReader(signature).acceptType(new SignatureDependencyVisitor());
    }

    void addConstant(Object constant) {
        if (constant instanceof Type) {
            addType((Type)constant);
        } else if (constant instanceof Handle) {
            Handle h = (Handle)constant;
            addCanonicalName(h.getOwner());
            addMethod(h.getDesc());
        }
    }
    
    class AnnotationDependencyVisitor extends AnnotationVisitor {
        AnnotationDependencyVisitor() {
            super(Opcodes.ASM5);
        }

        @Override
        public void visit(String name, Object value) {
            if (value instanceof Type)
                addType((Type)value);
        }

        @Override
        public void visitEnum(String name, String desc, String value) {
            addDesc(desc);
        }

        @Override
        public AnnotationVisitor visitAnnotation(String name, String desc) {
            addDesc(desc);
            return this;
        }

        @Override
        public AnnotationVisitor visitArray(String name) {
            return this;
        }
    }

    class FieldDependencyVisitor extends FieldVisitor {
        FieldDependencyVisitor() {
            super(Opcodes.ASM5);
        }

        @Override
        public AnnotationVisitor visitAnnotation(String desc, boolean visible) {
            addDesc(desc);
            return new AnnotationDependencyVisitor();
        }

        @Override
        public AnnotationVisitor visitTypeAnnotation(int typeRef, TypePath typePath, String desc, boolean visible) {
            addDesc(desc);
            return new AnnotationDependencyVisitor();
        }
    }

    class MethodDependencyVisitor extends MethodVisitor {
        MethodDependencyVisitor() {
            super(Opcodes.ASM5);
        }

        @Override
        public AnnotationVisitor visitAnnotationDefault() {
            return new AnnotationDependencyVisitor();
        }

        @Override
        public AnnotationVisitor visitAnnotation(String desc, boolean visible) {
            addDesc(desc);
            return new AnnotationDependencyVisitor();
        }

        @Override
        public AnnotationVisitor visitTypeAnnotation(int typeRef, TypePath typePath, String desc, boolean visible) {
            addDesc(desc);
            return new AnnotationDependencyVisitor();
        }

        @Override
        public AnnotationVisitor visitParameterAnnotation(int parameter, String desc, boolean visible) {
            addDesc(desc);
            return new AnnotationDependencyVisitor();
        }

        @Override
        public void visitTypeInsn(int opcode, String type) {
            addType(Type.getObjectType(type));
        }

        @Override
        public void visitFieldInsn(int opcode, String owner, String name, String desc) {
            addCanonicalName(owner);
            addDesc(desc);
        }

        @Override
        public void visitMethodInsn(int opcode, String owner, String name, String desc, boolean itf) {
            addCanonicalName(owner);
            addMethod(desc);
        }

        @Override
        public void visitInvokeDynamicInsn(String name, String desc, Handle bsm, Object... bsmArgs) {
            addMethod(desc);
            addConstant(bsm);
            for (Object bsmArg: bsmArgs)
                addConstant(bsmArg);
        }

        @Override
        public void visitLdcInsn(Object cst) {
            addConstant(cst);
        }

        @Override
        public void visitMultiANewArrayInsn(String desc, int dims) {
            addDesc(desc);
        }

        @Override
        public AnnotationVisitor visitInsnAnnotation(int typeRef, TypePath typePath, String desc, boolean visible) {
            addDesc(desc);
            return new AnnotationDependencyVisitor();
        }

        @Override
        public void visitLocalVariable(String name, String desc, String signature, Label start, Label end, int index) {
            addTypeSignature(signature);
        }

        @Override
        public AnnotationVisitor visitLocalVariableAnnotation(int typeRef, TypePath typePath, Label[] start, Label[] end, int[] index, String desc,
                boolean visible) {
            addDesc(desc);
            return new AnnotationDependencyVisitor();
        }

        @Override
        public void visitTryCatchBlock(Label start, Label end, Label handler, String type) {
            if (type != null) {
                addCanonicalName(type);
            }
        }

        @Override
        public AnnotationVisitor visitTryCatchAnnotation(int typeRef, TypePath typePath, String desc, boolean visible) {
            addDesc(desc);
            return new AnnotationDependencyVisitor();
        }
    }

    class SignatureDependencyVisitor extends SignatureVisitor {
        String signatureClassName;

        SignatureDependencyVisitor() {
            super(Opcodes.ASM5);
        }

        @Override
        public void visitClassType(String name) {
            signatureClassName = name;
            addCanonicalName(name);
        }

        @Override
        public void visitInnerClassType(String name) {
            signatureClassName = signatureClassName + "$" + name;
            addCanonicalName(signatureClassName);
        }
    }
}
