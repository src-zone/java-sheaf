/*
 * Copyright 2014 by The.Src.Zone (Amsterdam, The Netherlands)  
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except 
 * in compliance with the License. You may obtain a copy of the License at
 * 
 * http://the.src.zone/licenses/apache-2.0 
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the License 
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */
package zone.src.sheaf.properties;

import java.lang.reflect.InvocationTargetException;

class LogbackConfigurator {
    private static final String SLF4J_BRIDGE_INSTALL = "install";
    private static final String SLF4J_REMOVE_HANDLERS_FOR_ROOT_LOGGER = "removeHandlersForRootLogger";
    private static final String CLASS_SLF4J_BRIDGE_HANDLER = "org.slf4j.bridge.SLF4JBridgeHandler";

    private static boolean isInitialized;
    
    static synchronized void init() {
        if (!isInitialized) {
            isInitialized = true;
            installJulOverSlf4J();
        }
    }
    
    private static void installJulOverSlf4J() {
        try {
            //If jul-over-slf4j is on the classpath, install it:
            Class<?> bridgeHandler = Class.forName(CLASS_SLF4J_BRIDGE_HANDLER);
            Boolean installed = (Boolean)bridgeHandler.getMethod("isInstalled").invoke(null);
            if (!installed) {
                bridgeHandler.getDeclaredMethod(SLF4J_REMOVE_HANDLERS_FOR_ROOT_LOGGER).invoke(null);
                bridgeHandler.getDeclaredMethod(SLF4J_BRIDGE_INSTALL).invoke(null);
            }
        } catch (ClassNotFoundException e) {
            //Nothing to do, there's no jul-to-slf4j bridge installed on the classpath
        } catch (IllegalAccessException | InvocationTargetException  | NoSuchMethodException e) {
            throw new ApplicationPropertiesException("Unexpected exception, maybe a classpath or version problem for jul-to-slf4j", e);
        }
    }
}