/*
 * Copyright 2015 by The.Src.Zone (Amsterdam, The Netherlands)  
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except 
 * in compliance with the License. You may obtain a copy of the License at
 * 
 * http://the.src.zone/licenses/apache-2.0 
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the License 
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */
package zone.src.sheaf.properties;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class PropertiesInfo {
    // Note: please ABSOLUTELY NO LOGGING dependencies this class. It is
    // supposed to set properties before logging configuration is initialized.
    // this INCLUDES any import that transitively imports LOGGING...
    private static final Pattern EXPRESSION_PATTERN = Pattern.compile("\\$(?:\\{([A-Z0-9_.]+)(?:\\>([^}:]*))?(?:\\:([^}]*))?\\}|([A-Z0-9_]+))", Pattern.CASE_INSENSITIVE);
    private static final int EXPRESSION_VAR1 = 1;
    private static final int EXPRESSION_POSTFIX = 2;
    private static final int EXPRESSION_NOT_MATCHED = 3;
    private static final int EXPRESSION_VAR2 = 4;
    
    private static final String SHEAF_PREFIX_PROPERTIES = "sheaf-prefix.properties";

    private static final String FILE_DEFAULT_PROPERTIES = "default.properties";
    
    private final Properties properties;
    private final String prefix;
    private final Map<String, String> prefixMap;
    private String configDirString;
    private File configDir;
    private String runType;
    
    PropertiesInfo(Properties properties) {
        this(properties, SHEAF_PREFIX_PROPERTIES);
    }
    
    /**
     * Construct a new PropertiesInfo to customize for unit tests.
     * 
     * @param properties The propertiesInfo to copy. Usually you'll pass {@link ApplicationProperties#PROPS} here.
     */
    public PropertiesInfo(PropertiesInfo properties) {
        this.properties = new Properties(properties.properties);
        this.prefix = properties.prefix;
        this.prefixMap = properties.prefixMap;
        this.configDirString = properties.configDirString;
        this.configDir = properties.configDir;
        this.runType = properties.runType;
    }
    
    PropertiesInfo(Properties properties, String identificationResource) {
        this.properties = properties;
        //the prefix:
        String assignedPrefix = properties.getProperty(ApplicationProperties.PROPERTY_PREFIX);
        if (assignedPrefix == null)
            assignedPrefix = loadPrefixFromClasspath(identificationResource);
        prefix = assignedPrefix == null ? ApplicationProperties.DEFAULT_PREFIX : assignedPrefix.trim();
        prefixMap = Collections.unmodifiableMap(mapOf(ApplicationProperties.VAR_PREFIX, prefix));
        
        detectRunType();
        initConfigDir();
        loadProperties();
        thirdPartyConfigs();
    }
    
    private void detectRunType() {
        runType = get(ApplicationProperties.PROPERTY_RUNTYPE);
        if (runType == null) {
            boolean test = (isOnClassPath("org.junit.Test") || (isOnClassPath("org.junit.jupiter.api.Test") && isOnClassPath("org.junit.platform.launcher.core.DefaultLauncher")))
            		&& isInsideJunitRunner();
            // if not production and not testing we should be in debug
            runType = test ? ApplicationProperties.RUNTYPE_TEST : ApplicationProperties.RUNTYPE_DEBUG;
        }
    }

    private void initConfigDir() {
        // set a default configDir if none was specified in the system
        // properties:
        configDirString = get(ApplicationProperties.PROPERTY_CONFIG_DIR);
        if (configDirString == null) {
            configDir = new File(get(ApplicationProperties.PROPERTY_USER_HOME), "." + prefix + "config");
            configDirString = configDir.getPath();
        } else
            configDir = new File(configDirString);
    }
    
    private void loadProperties() {
        loadProperties(new File(configDir, runType + ".properties"), properties);
        loadProperties(new File(configDir, FILE_DEFAULT_PROPERTIES), properties);
    }
    
    private void thirdPartyConfigs() {
        // When using Spring and logback-sheaf together you typically don't want to use the Spring Logging System (SLS).
        // If you do want to use SLS together with logback-sheaf, just assign the Logging System to use to
        // "org.springframework.boot.logging.LoggingSystem" (picked up by Spring). Otherwise it will be
        // explicitly disabled, so that SLS won't compete with logback-sheaf for logging.
        if (PropertiesInfo.isOnClassPath(LoggingInfo.CLASS_LOGBACK_SHEAF_MARKER) &&
            properties.getProperty("org.springframework.boot.logging.LoggingSystem") == null)
            properties.setProperty("org.springframework.boot.logging.LoggingSystem", "none");
    }
    
    static boolean isOnClassPath(String clazz) {
        try {
            Class.forName(clazz);
            return true;
        } catch (ClassNotFoundException e) {
            return false;
        }
    }

    public String getPrefix() {
        return prefix;
    }
    
    public String getConfigDirString() {
        return configDirString;
    }
    
    public File getConfigDir() {
        return configDir;
    }
    
    public boolean isTesting() {
        return ApplicationProperties.RUNTYPE_TEST.equals(runType);
    }

    public boolean isDebug() {
        return ApplicationProperties.RUNTYPE_DEBUG.equals(runType);
    }

    public boolean isProduction() {
        return ApplicationProperties.RUNTYPE_PRODUCTION.equals(runType);
    }
    
    public String getRunType() {
        return runType;
    }

    public String get(String property) {
        return get(property, (Map<String, String>)null);
    }

    public String get(String prefix, String property) {
        return get(property, mapOf(ApplicationProperties.VAR_PREFIX, prefix));
    }

    public String get(String property, Map<String, String> substitutes) {
        substitutes = addPrefix(substitutes);
        return properties.getProperty(evaluateString(property, substitutes));
    }

    public void set(String property, String value) {
        set(property, value, (Map<String, String>)null);
    }

    public void set(String prefix, String property, String value) {
        set(property, value, mapOf(ApplicationProperties.VAR_PREFIX, prefix));
    }

    public void set(String property, String value, Map<String, String> substitutes) {
        substitutes = addPrefix(substitutes);
        property = evaluateString(property, substitutes);
        if (value == null)
            properties.remove(property);
        else
            properties.setProperty(property, value);
    }

    private Map<String, String> addPrefix(Map<String, String> substitutes) {
        if (substitutes == null)
            substitutes = prefixMap;
        else if (!substitutes.containsKey(ApplicationProperties.VAR_PREFIX)) {
            substitutes = new HashMap<String, String>(substitutes);
            substitutes.put(ApplicationProperties.VAR_PREFIX, prefix);
        }
        return substitutes;
    }

    static boolean isInsideJunitRunner() {
        return isInside("org.junit.runners.") || isInside("org.junit.platform.launcher.");
    }
    
    static boolean isInside(String prefix) {
        try {
            throw new IllegalStateException();
        } catch (IllegalStateException e) {
            for (StackTraceElement ste: e.getStackTrace())
                if (ste.getClassName().startsWith(prefix))
                    return true;
        }
        return false;
    }
    
    static String loadPrefixFromClasspath(String resource) {
        final Properties properties = loadMergedClasspathProperties(resource);
        String result = properties.getProperty(ApplicationProperties.PROPERTY_PREFIX);     
        if (result != null)
            result = result.trim();
        return result;

    }

    protected static Properties loadMergedClasspathProperties(String resource) {
        final Properties properties = new Properties();
        try {
            Collection<URL> urls = getClassPathResources(resource);
            for (URL url: urls) {
                Properties mergeProperties = new Properties();
                try (InputStream in = url.openStream()) {
                    mergeProperties.load(in);
                }
                mergeProperties.forEach((key, value) -> {
                    Object oldValue = properties.get(key);
                    if (oldValue != null && !oldValue.equals(value))
                        throw new ApplicationPropertiesException("Conflicting values for " + key
                                + " in different " + resource + " files on the classpath: "
                                + oldValue + ", " + value + " (loading from: " + urls + ")");
                    properties.put(key, value);
                });
            }
        } catch (IOException e) {
            throw new ApplicationPropertiesException("Error loading " + resource, e);
        }
        return properties;
    }

    private static Collection<URL> getClassPathResources(String resource) throws IOException {
        Enumeration<URL> urls = PropertiesInfo.class.getClassLoader().getResources(resource);
        Collection<URL> result = new ArrayList<>();
        while (urls.hasMoreElements()) {
            result.add(urls.nextElement());
        }
        return result;
    }

    static void loadProperties(File propertiesFile, Properties targetProperties) {
        Properties properties = new Properties();
        try (FileInputStream stream = new FileInputStream(propertiesFile)) {
            properties.load(stream);
            for (Object nameAsObj : properties.keySet()) {
                String name = (String)nameAsObj;
                if (targetProperties.getProperty(name) != null)
                    continue; // property was overridden
                targetProperties.setProperty(name, properties.getProperty(name));
            }
        } catch (FileNotFoundException e) {
            // properties not found, ignore (no logging as this is called during
            // logging initialization; we can log info about loaded properties
            // after the init)
        } catch (IOException e) {
            throw new ApplicationPropertiesException("Error loading system properties from: " + propertiesFile, e);
        }
    }

    private static Map<String, String> mapOf(String... elms) {
        HashMap<String, String> result = new HashMap<String, String>(elms.length / 2);
        for (int i = 0; i < elms.length; i += 2)
            result.put(elms[i], elms[i + 1]);
        return result;
    }
    
    static String evaluateString(String expression, Map<? extends CharSequence, ? extends CharSequence> substitutes) {
        StringBuilder result = new StringBuilder(expression.length() * 2);
        int idx = 0;
        Matcher matcher = EXPRESSION_PATTERN.matcher(expression);
        while (matcher.find()) {
            String var = matcher.group(EXPRESSION_VAR1);
            String notMatched = null;
            String postFix = null;
            if (var == null)
                var = matcher.group(EXPRESSION_VAR2);
            else {
                postFix = matcher.group(EXPRESSION_POSTFIX);
                notMatched = matcher.group(EXPRESSION_NOT_MATCHED);
            }
            result.append(expression.substring(idx, matcher.start()));
            idx = matcher.end();
            CharSequence replacement = substitutes != null ? substitutes.get(var) : null;
            if (replacement == null) {
                if (notMatched != null)
                    result.append(notMatched);
            } else {
                result.append(replacement);
                if (postFix != null)
                    result.append(postFix);
            }
        }
        result.append(expression.substring(idx));
        return result.toString();
    }
}
