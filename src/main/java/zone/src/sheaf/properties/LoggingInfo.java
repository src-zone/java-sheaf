/*
 * Copyright 2015 by The.Src.Zone (Amsterdam, The Netherlands)  
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except 
 * in compliance with the License. You may obtain a copy of the License at
 * 
 * http://the.src.zone/licenses/apache-2.0 
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the License 
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */
package zone.src.sheaf.properties;

import java.io.File;
import java.lang.reflect.Field;

final class LoggingInfo {
    static final String LOGBACK_CONFIGURATION_FILE = "logback.configurationFile";
    static final String CLASS_LOGBACK_SHEAF_MARKER = "zone.src.sheaf.logback.LogbackSheafMarker";
    private static final String CLASS_VELOCITY_RTC = "org.apache.velocity.runtime.RuntimeConstants";
    private static final String VELOCITY_RTC_LOGSYSTEM_CLASS = "RUNTIME_LOG_LOGSYSTEM_CLASS";
    private static final String CLASS_VELOCITY_SLF4J_LOGCHUTE = "zone.src.sheaf.velocity.Slf4jLogChute";
    
    private File logbackConfigFile;

    LoggingInfo(PropertiesInfo info) {
        if (PropertiesInfo.isOnClassPath(CLASS_LOGBACK_SHEAF_MARKER))
            initLogging(info);
    }
    
    File getLogbackConfigFile() {
        return logbackConfigFile;
    }
    
    private void initLogging(PropertiesInfo info) {
        String fileName = info.get(LOGBACK_CONFIGURATION_FILE);
        if (fileName == null) {
            logbackConfigFile = ApplicationProperties.getConfigFile(info.getConfigDir(), "logback", info.getRunType(), ".xml");
            if (logbackConfigFile != null)
                //set the logback.configurationFile for LogBack to pick up:
                info.set(LOGBACK_CONFIGURATION_FILE, logbackConfigFile.getAbsolutePath());
        } else
            logbackConfigFile = new File(fileName);
        LogbackConfigurator.init();
        installVelocitySlf4JLogChute();
    }
    
    private static void installVelocitySlf4JLogChute() {
        try {
            Class<?> velocity = Class.forName(CLASS_VELOCITY_RTC);
            Field field = velocity.getDeclaredField(VELOCITY_RTC_LOGSYSTEM_CLASS);
            String property = (String)field.get(null);
            System.setProperty(property, CLASS_VELOCITY_SLF4J_LOGCHUTE);
        } catch (ClassNotFoundException | NoSuchFieldException | IllegalAccessException e) {
            //Nothing to do: velocity not on classpath
        }
    }
}
