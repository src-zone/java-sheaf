/*
 * Copyright 2014 by The.Src.Zone (Amsterdam, The Netherlands)  
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except 
 * in compliance with the License. You may obtain a copy of the License at
 * 
 * http://the.src.zone/licenses/apache-2.0 
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the License 
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */
package zone.src.sheaf.properties;

import java.io.File;
import java.util.Map;

import zone.src.sheaf.utils.StringUtils;

/**
 * This class can be used to set application properties/configuration/settings
 * based on the type of run (test,debug,production, or a custom value), and configuration
 * and property files in an application specific initialization directory.
 * <p>
 * By default the configuration is read from the directory
 * <code>${user.home}/.zone.src.config</code>.
 * This can be overridden by setting system property zone.src.configDir to another path.
 * <p>
 * The <code>zone.src.</code> prefix for properties and for the name of the configuration
 * directory can be overridden in different ways (first match wins):
 * <ul>
 * <li>Set the system property <code>zone.src.prefix</code> (see {@link #DEFAULT_PREFIX}).</li>
 * <li>Have a file named <code>sheaf-prefix.properties</code> on the classpath with a property
 * definition for <code>zone.src.prefix</code></li>
 * </ul>
 * Note: You probably want to end your prefix with a dot!
 * <p>
 * During initialization this class will read the following property files from the
 * configuration directory {@link #CONFIG_DIR}, and load the properties in these files
 * as system properties:
 * <ul>
 * <li>default.properties</li>
 * <li><em>runtype</em>.properties (e.g. test.properties, debug.properties, or
 * production.properties). This may override properties in default.properties.</li>
 * </ul>
 * <p>
 * The library zone.src.sheaf:logback-sheaf on maven central is a drop in replacement for
 * ch.qos.logback:logback-classic. When using logback-sheaf, logback will be initialized
 * by ApplicationProperties as well. In that case, the logback configuration is loaded
 * from the configuration directory as follows:
 * <ul>
 * <li>Try to load logback-<em>runtype</em>.xml, e.g. logback-production.xml, logback-test.xml, or
 *     logback-debug.xml.</li>
 * <li>When the file logback-<em>runtype</em>.xml does not exist,
 *     try to load logback.xml instead.</li>
 * <li>When an appropriate logback configuration is still not found, the default mechanism of logback
 *     is used.</li>
 * </ul>
 * <p>
 * When using logback-sheaf AND having jul-to-slf4j on the classpath, ApplicationProperties will
 * automagically install the SLF4JBridgeHandler to your logback
 * configuration (see http://www.slf4j.org/api/org/slf4j/bridge/SLF4JBridgeHandler.html).
 * For optimal performance of the jul-to-slf4j bridge, you should add a
 * <a href="http://logback.qos.ch/manual/configuration.html#LevelChangePropagator">LevelChangePropagator</a>
 * context listener to your logback.xml, e.g.:
 * <pre>
 * &lt;configuration&gt;
 *   &lt;contextListener class="ch.qos.logback.classic.jul.LevelChangePropagator"&gt;
 *     &lt;resetJUL&gt;true&lt;/resetJUL&gt;
 *   &lt;/contextListener&gt;
 *    ....
 * &lt;/configuration&gt;
 * </pre>
 */
public class ApplicationProperties {
    // Note: please ABSOLUTELY NO LOGGING dependencies this class. It is
    // supposed to set properties before logging configuration is initialized.
    // this INCLUDES any import that transitively imports LOGGING...

    public static final String PROPERTY_USER_HOME = "user.home";
    
    /**
     * This property defines an (optional) property prefix for many property
     * names in this and other configuration classes. If not set, the default
     * property prefix {@link #DEFAULT_PREFIX} is used as prefix.
     * <p>
     * If {@link #PROPERTY_CONFIG_DIR} was not set, the config-dir is also
     * based on the property prefix: <code>${user.home}/.${zone.src.prefix}config</code>
     * <p>
     * Never alter this property in java code, it should be set during startup
     * of the JVM.
     */
    public static final String PROPERTY_PREFIX = "zone.src.prefix";
    
    /**
     * The default property prefix if no override is set via
     * {@link #PROPERTY_PREFIX}, or via a javasheaf.properties file on the classpath
     * with a property definition for {@link #PROPERTY_PREFIX}.
     */
    public static final String DEFAULT_PREFIX = "zone.src.";
    
    /**
     * The name of the special prefix var for property substitutions. See
     * {@link #get(String, Map)}.
     */
    public static final String VAR_PREFIX = "_";
    
    /**
     * This property defines the directory where configuration files (e.g. properties,
     * logging config, etc) are defined.
     */
    public static final String PROPERTY_CONFIG_DIR = "${_}configDir";
    
    /**
     * This property defines the runType (i.e. "test", "debug", or "production").
     */
    public static final String PROPERTY_RUNTYPE = "${_}runType";
    
    /**
     * The constant "test"
     */
    public static final String RUNTYPE_TEST = "test";

    /**
     * The constant "debug"
     */
    public static final String RUNTYPE_DEBUG = "debug";
    
    /**
     * The constant "production"
     */
    public static final String RUNTYPE_PRODUCTION = "production";
    
    public static final PropertiesInfo PROPS = new PropertiesInfo(System.getProperties());
    public static final LoggingInfo LOGGING = new LoggingInfo(PROPS);
    
    /**
     * @see #CONFIG_DIR
     */
    public static final String CONFIG_DIR_STRING = PROPS.getConfigDirString();
    
    /**
     * Configuration directory. Optionally contains
     * [production|debug|test].properties, logback[-debug\-test].xml and other
     * configuration files for your application.
     */
    public static final File CONFIG_DIR = PROPS.getConfigDir();
    
    /**
     * The actual property prefix. Is initialized from property
     * {@link #PROPERTY_PREFIX}, or the value {@link #DEFAULT_PREFIX}
     */
    public static final String PREFIX = PROPS.getPrefix();
    
    /**
     * The run-type (e.g. debug, test, or production). This influences which config
     * files and logback configuration will be loaded.
     */
    public static final String RUN_TYPE = PROPS.getRunType();
    
    /**
     * The file that was used for loading the logback configuration (if any), or null.
     */
    public static final File LOGBACK_CONFIG_FILE = LOGGING.getLogbackConfigFile();
    
    /**
     * Easy way to force proper initialization of {@link ApplicationProperties} before running tests that operate
     * on packaged parts of this package.
     */
    static void init() {
        // nothing to do here really...
    }

    /**
     * @param property A property to read, the property may contain ${_} for prefix substitution. 
     * @return The value for the given property
     */
    public static final String get(String property) {
        return PROPS.get(property);
    }

    /**
     * @param prefix The prefix to use for prefix substitution.
     * @param property A property to read, the property may contain ${_} for prefix substitution.
     * @return The value for the given property
     */
    public static final String get(String prefix, String property) {
        return PROPS.get(prefix, property);
    }

    /**
     * @param property A property to read, the property will first be processed with {@link StringUtils#evaluate(String, Map)}
     * @param substitutes The variable assignments to use during evaluation of the property string. If there is no assignment for
     *   the prefix variable (named <code>'_'</code>) the default prefix assignment will be added to the map. The map may be null,
     *   in which case it will be replaced by a map containing only an assignment for the default prefix.
     * @return The value for the given property
     */
    public static final String get(String property, Map<String, String> substitutes) {
        return PROPS.get(property, substitutes);
    }

    /**
     * Assign a system property a value, or remove the property when value is null. See {@link #get(String)}.
     * @param property The property, processed according to the rules of {@link #get(String)}
     * @param value The new value (if null: any existing property with the given name will be removed)
     */
    public static final void set(String property, String value) {
        PROPS.set(property, value);
    }

    /**
     * Assign a system property a value, or remove the property when value is null. See {@link #get(String, String)}
     * @param prefix The prefix value (for property name resolving of the var named: '_'.
     * @param property The property, processed according to the rules of {@link #get(String, String)}
     * @param value The new value
     */
    public static final void set(String prefix, String property, String value) {
        PROPS.set(prefix, property, value);
    }

    /**
     * Assign a system property a value, or remove the property when value is null. See {@link #get(String, Map)}
     * @param property The property, processed according to the rules of {@link #get(String, Map)}
     * @param value The new value
     * @param substitutes The substitutes for property name resolving 
     */
    public static final void set(String property, String value, Map<String, String> substitutes) {
        PROPS.set(property, value, substitutes);
    }

    /**
     * @return True when the code is (thought to be) executing as a unit test.
     *         With a test run the logback-test.xml and test.properties from the
     *         config directory should be applied.
     * 
     * @see #isDebug()
     * @see #isProduction()
     */
    public static boolean isTesting() {
        return PROPS.isTesting();
    }

    /**
     * @return True when the code is (thought to be) executing as a
     *         non-production standalone run (i.e. an execution launched by the
     *         development environment). In that case the logback-debug.xml and
     *         debug.properties from the config directory should be applied.
     * 
     * @see #isTesting()
     * @see #isProduction()
     */
    public static boolean isDebug() {
        return PROPS.isDebug();
    }

    /**
     * @return True when the code is executing as production standalone run
     *         (i.e. launched by the startup/management scripts of a
     *         distribution). In that case the logback.xml and
     *         production.properties from the config directory should be
     *         applied.
     * 
     * @see #isTesting()
     * @see #isDebug()
     */
    public static boolean isProduction() {
        return PROPS.isProduction();
    }
    
    /**
     * @return The run-type (e.g. debug, test, or production).
     * 
     * @see #isDebug()
     * @see #isTesting()
     * @see #isProduction()
     */
    public static String getRunType() {
        return RUN_TYPE;
    }
    
    /**
     * See {@link StringUtils#evaluate(String, Map)}
     * 
     * Implemented here so that {@link ApplicationProperties} has no dependencies on
     * other classes that may pull in logging dependencies.
     * 
     * @param expression The string expression to evaluate
     * @param substitutes The variable assignments to use during evaluation
     * 
     * @return The evaluation of the expression
     */
    public static String evaluateString(String expression, Map<? extends CharSequence, ? extends CharSequence> substitutes) {
        return PropertiesInfo.evaluateString(expression, substitutes);
    }
    
    public static File getConfigFile(File dir, String name, String specialization, String suffix) {
        File file = new File(dir, name + "-" + specialization + suffix);
        if (file.exists())
            return file;
        file = new File(dir, name + suffix);
        if (file.exists())
            return file;
        return null;
    }
}