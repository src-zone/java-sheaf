/*
 * Copyright 2015 by The.Src.Zone (Amsterdam, The Netherlands)  
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except 
 * in compliance with the License. You may obtain a copy of the License at
 * 
 * http://the.src.zone/licenses/apache-2.0 
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the License 
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */
package zone.src.sheaf.template.hierarchy;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import java.util.function.Function;

import org.junit.jupiter.api.Test;

import zone.src.sheaf.template.DefaultTemplateBindStrategy;
import zone.src.sheaf.template.TemplateBindStrategy;
import zone.src.sheaf.template.TemplateTypes;
import zone.src.sheaf.template.stubs.ClassA;
import zone.src.sheaf.template.stubs.ClassBextendsA;
import zone.src.sheaf.template.stubs.ClassC;
import zone.src.sheaf.template.stubs.ClassDextendsC;
import zone.src.sheaf.template.stubs.ClassEextendsD;

public class TemplateBindHierarchyTest {
    private static final NodeExtractor NODE_EXTRACTOR = new NodeExtractor();
    private static final String HTML = ".html";
    
    @Test public void empty() {
        TemplateBindHierarchy<Object> hierarchy = TemplateBindHierarchy.create(Object.class, TemplateTypes.HTML);
        assertThat(hierarchy.getBindChain(new ClassA())).isEmpty();
        assertThat(hierarchy.getBindRoot(new ClassA())).isNull();
    }
    
    @Test public void hierarchy() {
        TemplateBindStrategy strategy = new DefaultTemplateBindStrategy();
        TemplateBindHierarchy<ClassA> hierarchy = TemplateBindHierarchy.create(ClassA.class, TemplateTypes.HTML);
        ClassA obj1A = new ClassA();
        ClassBextendsA obj1BextendsA = new ClassBextendsA();

        hierarchy.add(strategy, obj1BextendsA);
        //ClassA is not a leaf:
        assertThat(hierarchy.getBindChain(obj1A)).isEmpty();
        assertThat(hierarchy.getBindRoot(obj1A)).isNull();
        //ClassBextendsA is a leaf with binding info:
        assertThat(hierarchy.getBindChain(obj1BextendsA)).extracting(NODE_EXTRACTOR).containsExactly(
            createBindNodeString(ClassA.class, HTML, null),
            createBindNodeString(ClassBextendsA.class, ".html", ClassA.class));
        assertThat(hierarchy.getBindRoot(obj1BextendsA).getClazz()).isEqualTo(ClassA.class);

        //Add ClassA as leaf:
        hierarchy.add(strategy, obj1A);
        assertThat(hierarchy.getBindRoot(obj1A).getClazz()).isEqualTo(ClassA.class);
        assertThat(hierarchy.getBindChain(obj1BextendsA)).extracting(NODE_EXTRACTOR).containsExactly(
            createBindNodeString(ClassA.class, HTML, null),
            createBindNodeString(ClassBextendsA.class, ".html", ClassA.class));
        assertThat(hierarchy.getBindChain(obj1A)).extracting(NODE_EXTRACTOR).containsExactly(
            createBindNodeString(ClassA.class, HTML, null));
    }
    
    @Test public void getLeafs() {
        TemplateBindStrategy strategy = new DefaultTemplateBindStrategy();
        TemplateBindHierarchy<ClassA> hierarchy = TemplateBindHierarchy.create(ClassA.class, TemplateTypes.HTML);
        ClassA obj1A = new ClassA();
        ClassA obj2A = new ClassA();
        ClassA obj3A = new ClassA();
        ClassBextendsA obj1BextendsA = new ClassBextendsA();
        ClassBextendsA obj2BextendsA = new ClassBextendsA();
        hierarchy.add(strategy, obj1BextendsA);
        hierarchy.add(strategy, obj1A);
        hierarchy.add(strategy, obj2A);
        hierarchy.add(strategy, obj3A);
        hierarchy.add(strategy, obj2BextendsA);
        
        assertThat(hierarchy.getLeafs()).containsExactly(obj1BextendsA, obj1A, obj2A, obj3A, obj2BextendsA);
    }
    
    @Test public void stopAtRootClass() {
        TemplateBindStrategy strategy = new DefaultTemplateBindStrategy();
        TemplateBindHierarchy<ClassBextendsA> hierarchy = TemplateBindHierarchy.create(ClassBextendsA.class, TemplateTypes.HTML);
        ClassBextendsA objBextendsA = new ClassBextendsA();

        hierarchy.add(strategy, objBextendsA);
        //Template of ClassA should not be used:
        assertThat(hierarchy.getBindRoot(objBextendsA).getClazz()).isEqualTo(ClassBextendsA.class);
    }
    
    @Test public void noMatchesInHierarchy() {
        TemplateBindStrategy strategy = new DefaultTemplateBindStrategy();
        TemplateBindHierarchy<ClassC> hierarchy = TemplateBindHierarchy.create(ClassC.class, TemplateTypes.HTML);
        ClassEextendsD objEextendsD = new ClassEextendsD();

        hierarchy.add(strategy, objEextendsD);
        //Only ClassDextendsC has a matching template
        assertThat(hierarchy.getBindRoot(objEextendsD).getClazz()).isEqualTo(ClassDextendsC.class);
        assertThat(hierarchy.getBindChain(objEextendsD)).extracting(NODE_EXTRACTOR).containsExactly(
            createBindNodeString(ClassDextendsC.class, HTML, null));        
    }
    
    @Test public void duplicates() {
        TemplateBindStrategy strategy = new DefaultTemplateBindStrategy();
        TemplateBindHierarchy<ClassA> hierarchy = TemplateBindHierarchy.create(ClassA.class, TemplateTypes.HTML);
        ClassA obj1A = new ClassA();
        ClassA obj2A = new ClassA();
        ClassBextendsA obj1BextendsA = new ClassBextendsA();
        ClassBextendsA obj2BextendsA = new ClassBextendsA();
        
        //should not throw:
        hierarchy.add(strategy, obj1A);
        hierarchy.add(strategy, obj2A);
        hierarchy.add(strategy, obj1BextendsA);
        hierarchy.add(strategy, obj2BextendsA);
        
        //duplicate:
        assertThatThrownBy(() -> {hierarchy.add(strategy, obj1A);}).isInstanceOf(IllegalArgumentException.class);
        assertThatThrownBy(() -> {hierarchy.add(strategy, obj1BextendsA);}).isInstanceOf(IllegalArgumentException.class);
    }
    
    public static String createBindNodeString(Class<?> clazz, String suffix, Class<?> parent) {
        String simpleName = clazz.getSimpleName();
        String parentName = parent == null ? null : parent.getSimpleName();
        return "TemplateBindNode [clazz=" + simpleName + ", strategy=DefaultTemplateBindStrategy [], templateUrl="
            + clazz.getResource(simpleName + suffix) + ", parent=" + parentName + "]";
    }
    
    public static class NodeExtractor implements Function<TemplateBindNode<?>, String> {
        @Override public String apply(TemplateBindNode<?> node) {
            return node.toString();
        }
    }
}