/*
 * Copyright 2015 by The.Src.Zone (Amsterdam, The Netherlands)  
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except 
 * in compliance with the License. You may obtain a copy of the License at
 * 
 * http://the.src.zone/licenses/apache-2.0 
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the License 
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */
package zone.src.sheaf.template.hierarchy;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import java.net.MalformedURLException;
import java.net.URL;

import org.junit.jupiter.api.Test;

import com.google.common.testing.EqualsTester;

import zone.src.sheaf.constants.FileExtensions;
import zone.src.sheaf.template.DefaultTemplateBindStrategy;
import zone.src.sheaf.template.TemplateBindStrategy;
import zone.src.sheaf.template.TemplateTypes;
import zone.src.sheaf.template.stubs.ClassA;
import zone.src.sheaf.template.stubs.ClassBextendsA;
import zone.src.sheaf.template.stubs.ClassC;
import zone.src.sheaf.template.stubs.ClassDextendsC;
import zone.src.sheaf.template.stubs.ClassEextendsD;

public class TemplateBindNodeTest {
    @Test public void properties() {
        TemplateBindStrategy strategy = new DefaultTemplateBindStrategy();
        TemplateBindHierarchy<ClassA> hierarchy = TemplateBindHierarchy.create(ClassA.class, TemplateTypes.HTML);
        ClassA objA = new ClassA();
        hierarchy.add(strategy, objA);
        TemplateBindNode<ClassA> node = hierarchy.getBindRoot(objA);
        assertThat(node.getClazz()).isEqualTo(ClassA.class);
        assertThat(node.getParent()).isNull();
        assertThat(node.getStrategy()).isSameAs(strategy);
        assertThat(node.getTemplateUrl()).isNotNull().isEqualTo(ClassA.class.getResource(ClassA.class.getSimpleName() + FileExtensions.HTML_SUFFIX));
    }
    
    @Test public void equalsAndHascode() throws MalformedURLException {
        TemplateBindStrategy strategy1 = new DefaultTemplateBindStrategy();
        TemplateBindStrategy strategy2 = new DefaultTemplateBindStrategy() {}; // different class -> different equality
        TemplateBindNode<ClassA> node1 = new TemplateBindNode<>(ClassA.class, strategy1, new URL("http://example.com/ClassA"));
        TemplateBindNode<ClassA> node2 = new TemplateBindNode<>(ClassA.class, strategy2, new URL("http://example.com/ClassA"));
        TemplateBindNode<ClassA> node3 = new TemplateBindNode<>(ClassA.class, strategy1, new URL("http://example.com/ClassA.2"));
        TemplateBindNode<ClassA> node4 = new TemplateBindNode<>(ClassBextendsA.class, strategy1, new URL("http://example.com/ClassA"));
        
        //Equality looks at Class & Strategy. The rest is ignored.
        new EqualsTester().addEqualityGroup(node1, node3).addEqualityGroup(node2).addEqualityGroup(node4).addEqualityGroup("string").testEquals();
    }
    
    @Test public void exceptionOnParentChange() throws MalformedURLException {
        TemplateBindStrategy strategy = new DefaultTemplateBindStrategy();
        TemplateBindNode<ClassC> node1 = new TemplateBindNode<>(ClassC.class, strategy, new URL("http://example.com/ClassC"));
        TemplateBindNode<ClassC> node2 = new TemplateBindNode<>(ClassDextendsC.class, strategy, new URL("http://example.com/ClassDextendsC"));
        TemplateBindNode<ClassC> node2b = new TemplateBindNode<>(ClassDextendsC.class, strategy, new URL("http://example.com/ClassDextendsC"));
        TemplateBindNode<ClassC> node3 = new TemplateBindNode<>(ClassEextendsD.class, strategy, new URL("http://example.com/ClassEextendsD"));
        
        node2.setParent(node1);
        node3.setParent(node2);
        
        assertThatThrownBy(() -> {node2.setParent(null);}).isInstanceOf(IllegalArgumentException.class);
        assertThatThrownBy(() -> {node3.setParent(node1);}).isInstanceOf(IllegalArgumentException.class);
        
        //setting to same parent IS allowed:
        node3.setParent(node2);
        
        //but just equal NOT:
        assertThatThrownBy(() -> {node3.setParent(node2b);}).isInstanceOf(IllegalArgumentException.class);
    }
}
