/*
 * Copyright 2015 by The.Src.Zone (Amsterdam, The Netherlands)  
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except 
 * in compliance with the License. You may obtain a copy of the License at
 * 
 * http://the.src.zone/licenses/apache-2.0 
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the License 
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */
package zone.src.sheaf.template.hierarchy;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;

import com.google.common.testing.EqualsTester;

import zone.src.sheaf.template.DefaultTemplateBindStrategy;
import zone.src.sheaf.template.TemplateTypes;
import zone.src.sheaf.template.stubs.ClassA;
import zone.src.sheaf.template.stubs.ClassBextendsA;
import zone.src.sheaf.template.stubs.ClassC;

public class DefaultTemplateBindStrategyTest {
    @Test public void getTemplate() {
        DefaultTemplateBindStrategy strategy = new DefaultTemplateBindStrategy();
        assertThat(strategy.getTemplate(ClassC.class, TemplateTypes.HTML)).isNull();
        assertThat(strategy.getTemplate(ClassA.class, TemplateTypes.HTML)).isNotNull().isEqualTo(ClassA.class.getResource("ClassA.html"));
        assertThat(strategy.getTemplate(ClassBextendsA.class, TemplateTypes.HTML)).isNotNull().isEqualTo(ClassA.class.getResource("ClassBextendsA.html"));
        assertThat(strategy.getTemplate(ClassA.class, TemplateTypes.JAVASCRIPT)).isNull();
        assertThat(strategy.getTemplate(ClassA.class, TemplateTypes.LESS)).isNull();
    }
    
    @Test public void equalsAndHasCode() {
        new EqualsTester().addEqualityGroup(new DefaultTemplateBindStrategy(), new DefaultTemplateBindStrategy())
            .addEqualityGroup(new OtherBindStrategy())
            .addEqualityGroup("string").testEquals();
    }
    
    public static final class OtherBindStrategy extends DefaultTemplateBindStrategy {}
}
