/*
 * Copyright 2014 by The.Src.Zone (Amsterdam, The Netherlands)  
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except 
 * in compliance with the License. You may obtain a copy of the License at
 * 
 * http://the.src.zone/licenses/apache-2.0 
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the License 
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */
package zone.src.sheaf.properties;

import static org.assertj.core.api.Assertions.assertThat;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.util.Map;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;

import com.google.common.collect.ImmutableMap;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import zone.src.sheaf.asm.DependencyVisitor;

public class ApplicationPropertiesTest {
    @Test public void detectRunType() {
        assertThat(ApplicationProperties.isTesting()).isTrue();
        assertThat(ApplicationProperties.isProduction()).isFalse();
        assertThat(ApplicationProperties.isDebug()).isFalse();
        assertThat(ApplicationProperties.getRunType()).isEqualTo(ApplicationProperties.RUNTYPE_TEST);
    }

    @Test public void getSetAndReadProperties() {
        assertThat(ApplicationProperties.get("myProperty")).isNull();
        assertThat(ApplicationProperties.get("${_}myProperty")).isNull();
        assertThat(ApplicationProperties.get("zone.src.myProperty")).isNull();
        assertThat(ApplicationProperties.get("myprefix.myProperty")).isNull();

        ApplicationProperties.set("${_}myProperty", "myValue1");
        ApplicationProperties.set("myProperty", "myValue2");
        ApplicationProperties.set("myprefix.", "${_}myProperty", "myValue3");

        assertThat(ApplicationProperties.get("myProperty")).isEqualTo("myValue2");
        assertThat(ApplicationProperties.get("prefix", "myProperty")).isEqualTo("myValue2");
        assertThat(ApplicationProperties.get("${_}myProperty")).isEqualTo("myValue1");
        assertThat(ApplicationProperties.get("zone.src.", "${_}myProperty")).isEqualTo("myValue1");
        assertThat(ApplicationProperties.get("zone.src.myProperty")).isEqualTo("myValue1");
        assertThat(ApplicationProperties.get("myprefix.myProperty")).isEqualTo("myValue3");
        assertThat(ApplicationProperties.get("myprefix.", "${_}myProperty")).isEqualTo("myValue3");

        ApplicationProperties.set("${_}myProperty", null);
        assertThat(ApplicationProperties.get("${_}myProperty")).isNull();
        
        ApplicationProperties.set("pre.${1}${2}.post", "value", ImmutableMap.of("1", "v1", "2", "v2"));
        assertThat(ApplicationProperties.get("pre.${1}${2}.post", ImmutableMap.of("1", "v1", "2", "v2"))).isEqualTo("value");
        assertThat(ApplicationProperties.get("pre.v1v2.post", (Map<String, String>)null)).isEqualTo("value");
    }

    @Test public void getPropertiesWithSubstitutions() {
        ApplicationProperties.set("${_}myProperty.a.prop1", "val1");
        ApplicationProperties.set("${_}myProperty.a1.prop1", "val2");
        ApplicationProperties.set("${_}myProperty.prop1", "val3");

        assertThat(ApplicationProperties.get("${_}myProperty.${subst}.prop1", ImmutableMap.of("subst", "a"))).isEqualTo("val1");
        assertThat(ApplicationProperties.get("${_}myProperty.$subst.prop1", ImmutableMap.of("x", "x", "subst", "a1", "y", "y"))).isEqualTo("val2");
        assertThat(ApplicationProperties.get("${_}myProperty${subst:.}prop1", (Map<String, String>)null)).isEqualTo("val3");
        assertThat(ApplicationProperties.get("${_}myProperty.${s:a1}.prop1", ImmutableMap.of("subst", ""))).isEqualTo("val2");
        assertThat(ApplicationProperties.get("${_}myProperty.${subst}.prop1", ImmutableMap.of("a", "b"))).isNull();
        assertThat(ApplicationProperties.get("${_}myProperty.prop1", (Map<String, String>)null)).isEqualTo("val3");
        assertThat(ApplicationProperties.get("${_}myProperty.prop1", ImmutableMap.of(ApplicationProperties.VAR_PREFIX, "perfix"))).isNull();
        assertThat(ApplicationProperties.get("${_}myProperty.prop1", ImmutableMap.of(ApplicationProperties.VAR_PREFIX, ApplicationProperties.PREFIX))).isEqualTo("val3");
    }
    
    @Test public void evaluateString() {
        assertThat(ApplicationProperties.evaluateString("xyz", null)).isEqualTo("xyz");
        //more testing of evaluateString in StringUtilsTest
    }

    private static final String[] ALLOWED_DEPENDENCY_PREFIXES = new String[] {
            "java.io.", "java.lang.", "java.net.", "java.util.",
            SuppressFBWarnings.class.getName(),
            ApplicationProperties.class.getPackage().getName() + "." };

    @Test public void dependencies() throws IOException {
        checkDependenciesFor(ApplicationProperties.class);
        checkDependenciesFor(PropertiesInfo.class);
        checkDependenciesFor(LoggingInfo.class);
    }
    
    @Test public void getConfigFile(@TempDir Path path) throws IOException {
    	  File dir = path.toFile();
        assertThat(ApplicationProperties.getConfigFile(dir, "name", "special", ".tst")).isNull();
        File defaultFile = new File(dir, "name.tst");
        defaultFile.createNewFile();
        assertThat(ApplicationProperties.getConfigFile(dir, "name", "special", ".tst")).isEqualTo(defaultFile);
        File specializedFile = new File(dir, "name-special.tst");
        specializedFile.createNewFile();
        assertThat(ApplicationProperties.getConfigFile(dir, "name", "special", ".tst")).isEqualTo(specializedFile);
    }

    protected void checkDependenciesFor(Class<?> sourceClazz) throws IOException {
        // No dependencies on anything that might initialize a logger, or other
        // parts that maight depend on ApplicationProperties:
        for (String clazz : DependencyVisitor.getDependencies(sourceClazz)) {
            boolean allowed = false;
            for (String prefix : ALLOWED_DEPENDENCY_PREFIXES)
                allowed = allowed || clazz.startsWith(prefix);
            assertThat(allowed).as("invalid dependency for " + sourceClazz.getSimpleName() + ": " + clazz).isTrue();
        }
    }
}
