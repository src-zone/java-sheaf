/*
 * Copyright 2015 by The.Src.Zone (Amsterdam, The Netherlands)  
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except 
 * in compliance with the License. You may obtain a copy of the License at
 * 
 * http://the.src.zone/licenses/apache-2.0 
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the License 
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */
package zone.src.sheaf.properties;

import static org.assertj.core.api.Assertions.assertThat;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.util.Properties;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;

import zone.src.sheaf.utils.StringUtils;

public class LoggingInfoTest {
    @Test
    @SuppressWarnings("checkstyle:methodlength") //testcase
    public void detectLogbackConfig(@TempDir Path tmp) throws IOException {
        File home = tmp.toFile();
        String prefix = "prefix.";
        String runtype = prefix + "runType";
        File configDir = new File(home, "." + prefix + "config");
        assertThat(configDir.mkdir()).isTrue();
        File logbackFile = new File(configDir, "logback.xml");
        File logbackTestFile = new File(configDir, "logback-test.xml");
        File logbackDebugFile = new File(configDir, "logback-debug.xml");
        
        Properties props = new Properties();
        props.setProperty(ApplicationProperties.PROPERTY_PREFIX, prefix);
        props.setProperty(ApplicationProperties.PROPERTY_USER_HOME, home.getAbsolutePath());
        LoggingInfo loggingInfo = new LoggingInfo(new PropertiesInfo(props));
        assertThat(loggingInfo.getLogbackConfigFile()).isNull();
        
        props.remove(LoggingInfo.LOGBACK_CONFIGURATION_FILE);
        props.setProperty(runtype, ApplicationProperties.RUNTYPE_DEBUG);
        loggingInfo = new LoggingInfo(new PropertiesInfo(props));
        assertThat(loggingInfo.getLogbackConfigFile()).isNull();
        
        props.remove(LoggingInfo.LOGBACK_CONFIGURATION_FILE);
        StringUtils.toFile("<?xml version=\"1.0\" encoding=\"UTF-8\"?><configuration></configuration>", logbackFile, StandardCharsets.UTF_8);
        loggingInfo = new LoggingInfo(new PropertiesInfo(props));
        assertThat(loggingInfo.getLogbackConfigFile().getName()).isEqualTo("logback.xml");
        
        props.remove(LoggingInfo.LOGBACK_CONFIGURATION_FILE);
        StringUtils.toFile("<?xml version=\"1.0\" encoding=\"UTF-8\"?><configuration></configuration>", logbackTestFile, StandardCharsets.UTF_8);
        StringUtils.toFile("<?xml version=\"1.0\" encoding=\"UTF-8\"?><configuration></configuration>", logbackDebugFile, StandardCharsets.UTF_8);
        loggingInfo = new LoggingInfo(new PropertiesInfo(props));
        assertThat(loggingInfo.getLogbackConfigFile().getName()).isEqualTo("logback-debug.xml");
        
        props.remove(LoggingInfo.LOGBACK_CONFIGURATION_FILE);
        props.setProperty(runtype, ApplicationProperties.RUNTYPE_PRODUCTION);
        loggingInfo = new LoggingInfo(new PropertiesInfo(props));
        assertThat(loggingInfo.getLogbackConfigFile().getName()).isEqualTo("logback.xml");
        
        props.remove(LoggingInfo.LOGBACK_CONFIGURATION_FILE);
        props.remove(runtype);
        loggingInfo = new LoggingInfo(new PropertiesInfo(props));
        assertThat(loggingInfo.getLogbackConfigFile().getName()).isEqualTo("logback-test.xml");
        
        //Use a specified file, even if that does not exist:
        props.setProperty(LoggingInfo.LOGBACK_CONFIGURATION_FILE, new File(configDir, "logback-nonexisting.xml").getAbsolutePath());
        loggingInfo = new LoggingInfo(new PropertiesInfo(props));
        assertThat(loggingInfo.getLogbackConfigFile().getName()).isEqualTo("logback-nonexisting.xml");        
    }
}