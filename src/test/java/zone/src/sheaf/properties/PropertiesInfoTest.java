/*
 * Copyright 2015 by The.Src.Zone (Amsterdam, The Netherlands)  
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except 
 * in compliance with the License. You may obtain a copy of the License at
 * 
 * http://the.src.zone/licenses/apache-2.0 
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the License 
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */
package zone.src.sheaf.properties;

import static org.assertj.core.api.Assertions.assertThat;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Properties;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;

import zone.src.sheaf.utils.StringUtils;

public class PropertiesInfoTest {
    @Test
    public void prefix() {
        Properties props = new Properties();
        PropertiesInfo info = new PropertiesInfo(props);
        assertThat(info.getPrefix()).isEqualTo(ApplicationProperties.DEFAULT_PREFIX);
        
        props = new Properties();
        props.setProperty(ApplicationProperties.PROPERTY_PREFIX, "myPrefix.");
        info = new PropertiesInfo(props);
        assertThat(info.getPrefix()).isEqualTo("myPrefix.");
        
        props = new Properties();
        info = new PropertiesInfo(props, "javasheaf-test.properties");
        assertThat(info.getPrefix()).isEqualTo("testPrefix.");
        
        props = new Properties();
        props.setProperty(ApplicationProperties.PROPERTY_PREFIX, "myPrefix.");
        info = new PropertiesInfo(props, "javasheaf-test.properties");
        assertThat(info.getPrefix()).isEqualTo("myPrefix.");
    }
    
    @Test
    public void configDir(@TempDir Path tmp) throws IOException {
        checkConfigDir(null, null, tmp);
        checkConfigDir("myPrefix.", null, tmp);
        checkConfigDir(null, Files.createTempFile(tmp, "tmp", null).toAbsolutePath().toString(), tmp);
        checkConfigDir("myPrefix.", Files.createTempFile(tmp, "tmp", null).toAbsolutePath().toString(), tmp);
    }
    
    protected void checkConfigDir(String prefix, String assignConfigDir, Path folder) throws IOException {
        Properties props = new Properties();
        File home = Files.createTempDirectory(folder, "home").toFile();
        props.setProperty(ApplicationProperties.PROPERTY_USER_HOME, home.getAbsolutePath());
        if (prefix != null)
            props.setProperty(ApplicationProperties.PROPERTY_PREFIX, prefix);
        String confDirProperty = (prefix == null ? ApplicationProperties.DEFAULT_PREFIX : prefix) + "configDir";
        if (assignConfigDir != null)
            props.setProperty(confDirProperty, assignConfigDir);
        PropertiesInfo info = new PropertiesInfo(props);
        File expected = new File(home, ".zone.src.config");
        if (prefix != null)
            expected = new File(home, "." + prefix + "config");
        if (assignConfigDir != null)
            expected = new File(assignConfigDir);
        assertThat(info.getConfigDir()).isEqualTo(expected);
        assertThat(info.getConfigDirString()).isEqualTo(expected.getAbsolutePath());
        assertThat(props.get(confDirProperty)).isEqualTo(assignConfigDir);
    }
    
    @Test
    public void detectRunType() {
        Properties props = new Properties();
        PropertiesInfo info = new PropertiesInfo(props);
        assertThat(info.isTesting()).isTrue();
        assertThat(info.isDebug()).isFalse();
        assertThat(info.isProduction()).isFalse();
        
        props = new Properties();
        props.setProperty("zone.src.runType", ApplicationProperties.RUNTYPE_DEBUG);
        info = new PropertiesInfo(props);
        assertThat(info.isTesting()).isFalse();
        assertThat(info.isDebug()).isTrue();
    }
    
    @Test
    public void loadProperties() throws IOException {
        Properties props = new Properties();
        props.setProperty("prop", "val");
        
        File propFile = createTestProperties("");
        propFile.delete();
        PropertiesInfo.loadProperties(propFile, props);
        assertThat(props).containsEntry("prop", "val").hasSize(1);
        
        propFile = createTestProperties("prop1=val1\nprop2=val2");
        PropertiesInfo.loadProperties(propFile, props);
        assertThat(props).containsEntry("prop", "val").containsEntry("prop1", "val1").containsEntry("prop2", "val2").hasSize(3);
        propFile.delete();

        propFile = createTestProperties("prop=val3");
        PropertiesInfo.loadProperties(propFile, props);
        // file should not overwrite existing properties!
        assertThat(props).containsEntry("prop", "val").containsEntry("prop1", "val1").containsEntry("prop2", "val2").hasSize(3);
        propFile.delete();
    }
    
    @Test
    public void loadPropertiesBasedOnRunType(@TempDir Path tmp) throws IOException {
        File configDir = tmp.toFile();
        File debugProps = new File(configDir, "debug.properties");
        File testProps = new File(configDir, "test.properties");
        File prodProps = new File(configDir, "production.properties");
        File defaultProps = new File(configDir, "default.properties");
        
        StringUtils.toFile("type=debug\ndebug=yes", debugProps, StandardCharsets.UTF_8);
        StringUtils.toFile("type=test\ntest=yes", testProps, StandardCharsets.UTF_8);
        StringUtils.toFile("type=production\nproduction=yes", prodProps, StandardCharsets.UTF_8);
        StringUtils.toFile("type=default\ndefault=yes", defaultProps, StandardCharsets.UTF_8);
        
        checkLoadProps(configDir, null);
        checkLoadProps(configDir, ApplicationProperties.RUNTYPE_TEST);
        checkLoadProps(configDir, ApplicationProperties.RUNTYPE_DEBUG);
        checkLoadProps(configDir, ApplicationProperties.RUNTYPE_PRODUCTION);
    }
    
    @Test
    public void isInside() {
        assertThat(PropertiesInfo.isInsideJunitRunner()).isTrue();
        assertThat(PropertiesInfo.isInside(PropertiesInfoTest.class.getName())).isTrue();
        assertThat(PropertiesInfo.isInside(ApplicationPropertiesTest.class.getName())).isFalse();
    }
    
    @Test
    public void onClassPath() {
        assertThat(PropertiesInfo.isOnClassPath(getClass().getName())).isTrue();
        assertThat(PropertiesInfo.isOnClassPath("org.junit.platform.launcher.core.DefaultLauncher")).isTrue();
        assertThat(PropertiesInfo.isOnClassPath("org.junit.jupiter.api.Test")).isTrue();
        assertThat(PropertiesInfo.isOnClassPath("org.junit.ThisIsNotAClass")).isFalse();
    }
    
    @Test
    public void disableSpringLoggingSystemByDefault() {
        Properties props = new Properties();
        PropertiesInfo info = new PropertiesInfo(props);
        // by default tell spring to shut-up with its logging system that always misses bootstrap logs:
        assertThat(info.get("org.springframework.boot.logging.LoggingSystem")).isEqualTo("none");
        
        props = new Properties();
        props.setProperty("org.springframework.boot.logging.LoggingSystem", "org.springframework.boot.logging.java.JavaLoggingSystem");
        info = new PropertiesInfo(props);
        // don't override a specific configured value if it has been set:
        assertThat(info.get("org.springframework.boot.logging.LoggingSystem")).isEqualTo("org.springframework.boot.logging.java.JavaLoggingSystem");
    }

    protected void checkLoadProps(File configDir, String runType) {
        Properties props = new Properties();
        if (runType != null)
            props.setProperty("zone.src.runType", runType);
        props.setProperty("zone.src.configDir", configDir.getAbsolutePath());
        PropertiesInfo info = new PropertiesInfo(props);
        String type = runType == null ? ApplicationProperties.RUNTYPE_TEST : runType;
        assertThat(info.get("type")).isEqualTo(type);
        assertThat(info.get("default")).isEqualTo("yes");
        assertThat(info.get(type)).isEqualTo("yes");
    }
    
    protected File createTestProperties(CharSequence content) throws IOException {
        File result = File.createTempFile("tstprop", ".properties");
        result.deleteOnExit();
        com.google.common.io.Files.asCharSink(result, StandardCharsets.UTF_8).write(content);
        return result;
    }
}
