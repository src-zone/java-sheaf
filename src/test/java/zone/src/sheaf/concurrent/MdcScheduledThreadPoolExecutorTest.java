/*
 * Copyright 2016 by The.Src.Zone (Amsterdam, The Netherlands)  
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except 
 * in compliance with the License. You may obtain a copy of the License at
 * 
 * http://the.src.zone/licenses/apache-2.0 
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the License 
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */
package zone.src.sheaf.concurrent;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.verify;

import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor.AbortPolicy;
import java.util.concurrent.ThreadPoolExecutor.DiscardPolicy;
import java.util.concurrent.TimeUnit;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;

import com.google.common.collect.ImmutableMap;
import com.google.common.util.concurrent.ThreadFactoryBuilder;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.Logger;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.Appender;

@ExtendWith(MockitoExtension.class)
public class MdcScheduledThreadPoolExecutorTest {
    private static final org.slf4j.Logger LOG = LoggerFactory.getLogger(MdcThreadPoolExecutor.class);
    @Mock private Appender<ILoggingEvent> mockAppender;
    @Captor ArgumentCaptor<ILoggingEvent> captorLoggingEvent;
    
    @BeforeEach public void setup() {
        Logger logger = (Logger)LoggerFactory.getLogger(Logger.ROOT_LOGGER_NAME);
        logger.addAppender(mockAppender);
    }
    
    @AfterEach public void teardown() {
        Logger logger = (Logger) LoggerFactory.getLogger(Logger.ROOT_LOGGER_NAME);
        logger.detachAppender(mockAppender);
    }
    
    @Test public void construct() {
        ThreadFactory threadFactory = new ThreadFactoryBuilder().setDaemon(true).build();
        
        MdcScheduledThreadPoolExecutor executor = new MdcScheduledThreadPoolExecutor(2);
        assertThat(executor.getCorePoolSize()).isEqualTo(2);
        assertThat(executor.getRejectedExecutionHandler()).isInstanceOf(AbortPolicy.class);
        assertThat(executor.getThreadFactory()).isNotNull().isNotEqualTo(threadFactory);
        
        executor = new MdcScheduledThreadPoolExecutor(2, new DiscardPolicy());
        assertThat(executor.getCorePoolSize()).isEqualTo(2);
        assertThat(executor.getRejectedExecutionHandler()).isInstanceOf(DiscardPolicy.class);
        assertThat(executor.getThreadFactory()).isNotNull().isNotEqualTo(threadFactory);
        
        executor = new MdcScheduledThreadPoolExecutor(2, threadFactory, new DiscardPolicy());
        assertThat(executor.getCorePoolSize()).isEqualTo(2);
        assertThat(executor.getRejectedExecutionHandler()).isInstanceOf(DiscardPolicy.class);
        assertThat(executor.getThreadFactory()).isEqualTo(threadFactory);
        
        executor = new MdcScheduledThreadPoolExecutor(2, threadFactory);
        assertThat(executor.getCorePoolSize()).isEqualTo(2);
        assertThat(executor.getRejectedExecutionHandler()).isInstanceOf(AbortPolicy.class);
        assertThat(executor.getThreadFactory()).isEqualTo(threadFactory);        
    }
    
    @Test public void logScheduledMdc() throws InterruptedException, ExecutionException {
        MdcScheduledThreadPoolExecutor executor = new MdcScheduledThreadPoolExecutor(1);
        try {
            MDC.put("var", "exec1");
            LOG.info("logline");
            verifyLogLine(Level.INFO, "logline", ImmutableMap.of("var", "exec1"));
            
            for (int i = 0; i != 10; ++i) {
                MDC.put("var", "exec" + i);
                boolean[] mdcCorrect = new boolean[1];
                final int nr = i;
                executor.schedule(new Callable<Void>() {
                    public Void call() {
                        LOG.info("logline");
                        mdcCorrect[0] = ("exec" + nr).equals(MDC.get("var"));
                        return null;
                    }
                }, 1L, TimeUnit.MILLISECONDS).get();
                assertThat(mdcCorrect[0]).isTrue();
                verifyLogLine(Level.INFO, "logline", ImmutableMap.of("var", "exec" + i));
            }
        } finally {
            executor.shutdown();
            executor.awaitTermination(100, TimeUnit.MILLISECONDS);
        }
    }
    
    @Test public void logMdc() throws InterruptedException, ExecutionException {
        MdcScheduledThreadPoolExecutor executor = new MdcScheduledThreadPoolExecutor(2);
        try {
            MDC.put("var", "exec1");
            LOG.info("logline");
            verifyLogLine(Level.INFO, "logline", ImmutableMap.of("var", "exec1"));
            
            for (int i = 0; i != 10; ++i) {
                MDC.put("var", "exec" + i);
                boolean[] mdcCorrect = new boolean[1];
                final int nr = i;
                executor.submit(new Callable<Void>() {
                    public Void call() {
                        LOG.info("logline");
                        mdcCorrect[0] = ("exec" + nr).equals(MDC.get("var"));
                        return null;
                    }
                }).get();
                assertThat(mdcCorrect[0]).isTrue();
                verifyLogLine(Level.INFO, "logline", ImmutableMap.of("var", "exec" + i));
            }
        } finally {
            executor.shutdown();
            executor.awaitTermination(100, TimeUnit.MILLISECONDS);
        }
    }

    @SuppressWarnings("unchecked")
    protected void verifyLogLine(Level level, String message, Map<String, String> mdc) {
        verify(mockAppender).doAppend(captorLoggingEvent.capture());
        //Having a genricised captor means we don't need to cast
        ILoggingEvent loggingEvent = captorLoggingEvent.getValue();
        //Check log level is correct
        assertThat(loggingEvent.getLevel()).isEqualTo(level);
        assertThat(loggingEvent.getFormattedMessage()).isEqualTo(message);
        assertThat(loggingEvent.getMDCPropertyMap()).containsAllEntriesOf(mdc).containsOnlyKeys(mdc.keySet().toArray(new String[mdc.size()]));
        reset(mockAppender);
    }
}
