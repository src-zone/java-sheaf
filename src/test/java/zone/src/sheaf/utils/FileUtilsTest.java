/*
 * Copyright 2014 by The.Src.Zone (Amsterdam, The Netherlands)  
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except 
 * in compliance with the License. You may obtain a copy of the License at
 * 
 * http://the.src.zone/licenses/apache-2.0 
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the License 
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */
package zone.src.sheaf.utils;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;

public class FileUtilsTest {
    @Test
    public void deleteRecursiveFile() throws IOException {
        File file = File.createTempFile("tstDelete", ".tst");
        file.deleteOnExit();
        assertThat(file).exists();
        FileUtils.deleteRecursive(file.toPath());
        assertThat(file).doesNotExist();
        //throw if file/path doesn't exist:
        assertThatThrownBy(() -> {FileUtils.deleteRecursive(file.toPath());}).isInstanceOf(IOException.class);
    }
    
    @Test
    public void deleteRecursive(@TempDir Path tempDir) throws IOException {
        File file = new File(tempDir.toFile(), "dir");
        assertThat(file.mkdir()).isTrue();
        try {
            assertThat(new File(file, "file1").createNewFile()).isTrue();
            File dir1 = new File(file, "dir1");
            File dir2 = new File(dir1, "dir2");
            assertThat(dir1.mkdir()).isTrue();
            assertThat(dir2.mkdir()).isTrue();
            assertThat(new File(dir1, "file2.txt").createNewFile()).isTrue();
            assertThat(new File(dir2, "file3.xml").createNewFile()).isTrue();
        } finally {
            assertThat(file).isDirectory();
            FileUtils.deleteRecursive(file.toPath());
            assertThat(file).doesNotExist();
        }
        //delete again should not succeed:
        assertThatThrownBy(() -> {FileUtils.deleteRecursive(file.toPath());}).isInstanceOf(IOException.class);
    }
    
    @Test
    public void deleteRecursiveNullForPath() throws IOException {
    	assertThatThrownBy(() -> {
    		FileUtils.deleteRecursive((Path)null);
    	}).isInstanceOf(NullPointerException.class);
    }

    @Test
    public void deleteRecursiveNullForFile() throws IOException {
    	assertThatThrownBy(() -> {
    		FileUtils.deleteRecursive((File)null);
    	}).isInstanceOf(NullPointerException.class);
    }
}
