/*
 * Copyright 2014 by The.Src.Zone (Amsterdam, The Netherlands)  
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except 
 * in compliance with the License. You may obtain a copy of the License at
 * 
 * http://the.src.zone/licenses/apache-2.0 
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the License 
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */
package zone.src.sheaf.utils;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.assertj.core.api.Assertions.fail;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.io.ByteArrayOutputStream;
import java.io.CharArrayWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.Writer;
import java.nio.CharBuffer;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.HashMap;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;
import org.mockito.ArgumentMatchers;

import com.google.common.collect.ImmutableMap;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;

@SuppressFBWarnings("NP_NULL_PARAM_DEREF_NONVIRTUAL") // for tests
public class StringUtilsTest {
    @Test public void evaluateNull() {    
        assertThatThrownBy(() -> {
        	StringUtils.evaluate(null, ImmutableMap.of("a", "b"));
        }).isInstanceOf(NullPointerException.class);
    }
    
    @Test public void evaluate() {
        assertThat(StringUtils.evaluate("aap", null)).isEqualTo("aap");
        assertThat(StringUtils.evaluate("aap", ImmutableMap.of("a", "b"))).isEqualTo("aap");
        assertThat(StringUtils.evaluate("$aap", ImmutableMap.of("a", "b"))).isEqualTo("");
        assertThat(StringUtils.evaluate("$a$ap", ImmutableMap.of("a", "b"))).isEqualTo("b");
        assertThat(StringUtils.evaluate("123${animal:.}$name.$animal", ImmutableMap.of("animal", "monkey", "name", "peter"))).isEqualTo("123monkeypeter.monkey");
        assertThat(StringUtils.evaluate("123${animal:.}$name.$animal", ImmutableMap.of("name", "peter"))).isEqualTo("123.peter.");
        assertThat(StringUtils.evaluate("${animal:.}$name.$animal", new HashMap<String, String>())).isEqualTo("..");
        assertThat(StringUtils.evaluate("${animal:.}$name.$animal", null)).isEqualTo("..");
        assertThat(StringUtils.evaluate("${animal>post:empty}", ImmutableMap.of())).isEqualTo("empty");
        assertThat(StringUtils.evaluate("${animal>post:empty}", ImmutableMap.of("animal", "monkey"))).isEqualTo("monkeypost");
        assertThat(StringUtils.evaluate("${animal>post:empty}", ImmutableMap.of("animal", ""))).isEqualTo("post");
    }
    
    @Test public void evaluateNamesWithDot() {
        assertThat(StringUtils.evaluate("${aap.noot}$aap.noot", ImmutableMap.of("aap.noot", "mies"))).isEqualTo("mies.noot");
        assertThat(StringUtils.evaluate("${aap.noot}$aap.noot", ImmutableMap.of("aap.noot", "mies", "aap", "dier"))).isEqualTo("miesdier.noot");
        assertThat(StringUtils.evaluate("${aap.noot:.}$aap.noot", ImmutableMap.of("aap.noot", "mies"))).isEqualTo("mies.noot");
        assertThat(StringUtils.evaluate("${aap.noot:.}$aap.noot", ImmutableMap.of("aap.noot", "mies", "aap", "dier"))).isEqualTo("miesdier.noot");
    }
    
    @Test public void urlDecodeNull() {
        assertThatThrownBy(() -> {
            StringUtils.urlDecode(null);
        }).isInstanceOf(NullPointerException.class);
    }
    
    @Test public void urlDecode() {
        assertThat(StringUtils.urlDecode("encode+this%20%24%40%23!~-12.%2C%3F%2B%2F")).isEqualTo("encode this $@#!~-12.,?+/");
    }
    
    @Test public void urlDecodeInvalidEnc() {
        assertThatThrownBy(() -> {
        	StringUtils.urlDecode("encode+this%20%24%40%23!~-12.%2C%3F%2B%2F", "INVALID");
        }).isInstanceOf(IllegalStateException.class);
    }
    
    @Test public void urlEncodeNull() {
        assertThatThrownBy(() -> {
        	StringUtils.urlEncode(null);
        }).isInstanceOf(NullPointerException.class);
    }
    
    @Test public void urlEncode() {
        assertThat(StringUtils.urlEncode("encode this $@#!~-12.,?+/")).isEqualTo("encode+this+%24%40%23%21%7E-12.%2C%3F%2B%2F");
    }
    
    @Test public void urlEncodeInvalidEnc() {
        assertThatThrownBy(() -> {
        	 StringUtils.urlEncode("encode this $@#!~-12.,?+/", "INVALID");
        }).isInstanceOf(IllegalStateException.class);
    }
    
    @Test public void toStringFileNotExists(@TempDir Path tmp) throws IOException {
        assertThatThrownBy(() -> {
        	StringUtils.toString(new File(tmp.toFile(), "doesnotexist.txt"), StandardCharsets.UTF_8);
        }).isInstanceOf(FileNotFoundException.class);
    }
    
    @Test public void testToString(@TempDir Path tmp) throws IOException {
        String testContent = "test content\nline2";
        File testFile = newFile(tmp, "test.txt");
        StringUtils.toFile(testContent, testFile, StandardCharsets.UTF_8);
        
        assertThat(StringUtils.toString(testFile, StandardCharsets.UTF_8)).isEqualTo(testContent);
        
        final FileInputStream stream1 = new FileInputStream(testFile);
        assertThat(StringUtils.toString(stream1, StandardCharsets.UTF_8)).isEqualTo(testContent);
        assertThatThrownBy(() -> {
            stream1.read();
        }).isInstanceOf(IOException.class);
        
        final FileInputStream stream2 = new FileInputStream(testFile);
        assertThat(StringUtils.toString(stream2, StandardCharsets.UTF_8, false)).isEqualTo(testContent);
        try {
            stream2.read();
        } catch (IOException e) {
            fail("stream.read should succeed, because the stream should not have been closed");
        } finally {
            stream2.close();
        }
    }
    
    @Test public void toStringUrl(@TempDir Path tmp) throws IOException {
        String testContent = "test content\nline2";
        File testFile = newFile(tmp, "test.txt");
        StringUtils.toFile(testContent, testFile, StandardCharsets.UTF_8);
        
        assertThat(StringUtils.toString(testFile.toURI().toURL(), StandardCharsets.UTF_8)).isEqualTo(testContent);
    }

    @Test public void toStringReader(@TempDir Path tmp) throws IOException {
        String testContent = "test content\nline2";
        File testFile = newFile(tmp, "test.txt");
        StringUtils.toFile(testContent, testFile, StandardCharsets.UTF_8);
        
        final FileReader reader1 = new FileReader(testFile);
        assertThat(StringUtils.toString(reader1)).isEqualTo(testContent);
        assertThatThrownBy(() -> {reader1.read();}).isInstanceOf(IOException.class);
        
        final FileReader reader2 = new FileReader(testFile);
        assertThat(StringUtils.toString(reader2, false)).isEqualTo(testContent);
        try {
            reader2.read();
        } catch (IOException e) {
            fail("reader.read should succeed, because the stream should not have been closed");
        } finally {
            reader2.close();
        }
    }
    
    @SuppressWarnings("checkstyle:methodlength") //unit-test
    @Test public void toStringReaderThrowing(@TempDir Path tmp) throws IOException {
        String testContent = "test content\nline2";
        File testFile = newFile(tmp, "test.txt");
        StringUtils.toFile(testContent, testFile, StandardCharsets.UTF_8);
        
        FileReader orgReader = new FileReader(testFile);
        FileReader reader1 = spy(orgReader);
        try {
          doReturn(-1).when(reader1).read(any(), anyInt(), anyInt());
	        doThrow(new IOException("stub close")).when(reader1).close();
	        assertThatThrownBy(() -> {
	            StringUtils.toString(reader1);
	        }).isInstanceOf(IOException.class).hasMessage("stub close");
	        verify(reader1, times(1)).close();
        } finally {
        	orgReader.close();
        }
        
        orgReader = new FileReader(testFile);
        FileReader reader2 = spy(orgReader);
        try {
          doThrow(new IOException("read failure")).when(reader2).read(any(), anyInt(), anyInt());
          doThrow(new IOException("read failure")).when(reader2).read(ArgumentMatchers.any(CharBuffer.class));
          doThrow(new IOException("read failure")).when(reader2).read(ArgumentMatchers.any(char[].class));
          doNothing().when(reader2).close();
          assertThatThrownBy(() -> {
              StringUtils.toString(reader2);
          }).isInstanceOf(IOException.class).hasMessage("read failure");
          verify(reader2, times(1)).close();
        } finally {
          orgReader.close();
        }
        
        orgReader = new FileReader(testFile);
        FileReader reader3 = spy(orgReader);
        try {
          doThrow(new IOException("read failure")).when(reader3).read(any(), anyInt(), anyInt());
	        doThrow(new IOException("read failure")).when(reader3).read(ArgumentMatchers.any(CharBuffer.class));
	        doThrow(new IOException("read failure")).when(reader3).read(ArgumentMatchers.any(char[].class));
	        doThrow(new IOException("close failure")).when(reader3).close();
	        assertThatThrownBy(() -> {
	            StringUtils.toString(reader3);
	        }).isInstanceOf(IOException.class).hasMessage("read failure"); // read failure will be thrown, close failure ignored
	        verify(reader3, times(1)).close();
        } finally {
        	orgReader.close();
        }
        
        orgReader = new FileReader(testFile);
        FileReader reader4 = spy(orgReader);
        try {
          doThrow(new IOException("read failure")).when(reader4).read(any(), anyInt(), anyInt());
	        doThrow(new IOException("read failure")).when(reader4).read(ArgumentMatchers.any(CharBuffer.class));
	        doThrow(new IOException("read failure")).when(reader4).read(ArgumentMatchers.any(char[].class));
	        doNothing().when(reader4).close();
	        assertThatThrownBy(() -> {
	            StringUtils.toString(reader4, false);
	        }).isInstanceOf(IOException.class).hasMessage("read failure"); // read failure will be thrown
	        verify(reader4, times(0)).close();
        } finally {
        	orgReader.close();
        }
    }
    
    @Test public void toStream() throws IOException {
        String testContent = "test content\nline2";
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        
        StringUtils.toStream(testContent, out, StandardCharsets.UTF_8);
        assertThat(out.toString(StandardCharsets.UTF_8.name())).isEqualTo(testContent);
    }
    
    @Test public void toWriter() throws IOException {
        String testContent = "test content\nline2";
        CharArrayWriter writer = new CharArrayWriter();

        StringUtils.toWriter(testContent, writer);
        assertThat(writer.toString()).isEqualTo(testContent);
    }
    
    @SuppressWarnings("checkstyle:methodlength") //unit-test
    @Test public void toWriterThrowing() throws IOException {
        final Writer writer1 = mock(Writer.class);
        doReturn(writer1).when(writer1).append(ArgumentMatchers.any());
        doThrow(new IOException("stub close")).when(writer1).close();
        assertThatThrownBy(() -> {
            StringUtils.toWriter("test", writer1);
        }).isInstanceOf(IOException.class).hasMessage("stub close");
        verify(writer1, times(1)).close();
        
        final Writer writer2 = mock(Writer.class);
        doThrow(new IOException("write failure")).when(writer2).append(ArgumentMatchers.any());
        assertThatThrownBy(() -> {
            StringUtils.toWriter("test", writer2);
        }).isInstanceOf(IOException.class).hasMessage("write failure");
        verify(writer2, times(1)).close();
        
        final Writer writer3 = mock(Writer.class);
        doThrow(new IOException("write failure")).when(writer3).append(ArgumentMatchers.any(CharSequence.class));
        doThrow(new IOException("close failure")).when(writer3).close();
        assertThatThrownBy(() -> {
            StringUtils.toWriter("test", writer3);
        }).isInstanceOf(IOException.class).hasMessage("write failure");
        verify(writer3, times(1)).close();
        
        final Writer writer4 = mock(Writer.class);
        doThrow(new IOException("write failure")).when(writer4).append(ArgumentMatchers.any(CharSequence.class));
        assertThatThrownBy(() -> {
            StringUtils.toWriter("test", writer4, false);
        }).isInstanceOf(IOException.class).hasMessage("write failure");
        verify(writer4, times(0)).close();
    }
    
    @Test public void unquote() throws IOException {
        assertThat(StringUtils.unquote(null)).isNull();
        assertThat(StringUtils.unquote("")).isEqualTo("");
        assertThat(StringUtils.unquote("\"")).isEqualTo("\"");
        assertThat(StringUtils.unquote("aaa")).isEqualTo("aaa");
        assertThat(StringUtils.unquote("\"\"")).isEqualTo("");
        assertThat(StringUtils.unquote("\"a b c\"")).isEqualTo("a b c");
        assertThat(StringUtils.unquote("''")).isEqualTo("");
        assertThat(StringUtils.unquote("'a b c'")).isEqualTo("a b c");
        assertThat(StringUtils.unquote("''''")).isEqualTo("''");
        assertThat(StringUtils.unquote("'\"")).isEqualTo("'\"");
    }
    
    private File newFile(Path folder, String name) throws IOException {
    	return Files.createFile(new File(folder.toFile(), name).toPath()).toFile();
    }
}